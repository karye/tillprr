#!/usr/bin/python3

import glob, os, shutil, re, json, html, urllib.parse

# Delete docs/*
if os.path.exists(doc_root):
    shutil.rmtree(doc_root)
    print("... deleted: " + doc_root)

print("Starting...")

# Assets folder
assets_folder = ".gitbook/assets/"

# Tabsize in spaces
tabsize = 2

# Load assets.json
assets_dict = {}
with open("assets.json", "r") as f:
    assets_dict = json.load(f)

# Find all md-pages all folders and iterate over them
counter = 1
for md_file in glob.glob("**/*.md", recursive=True):

    # Open md-file
    with open(md_file, "r") as f:
        # Read md-file
        content = f.read()
        print("\n... processing: " + md_file)

        # Find images and replace them from assets_dict
        for key, value in assets_dict.items():
            # Replace images
            content = content.replace(key, value)

        # Write md-file
        with open(md_file, "w") as f:
            f.write(content)

# Rename images in assets-folder
if os.path.exists(assets_folder):

    # Iterate over all images
    for img in glob.glob(assets_folder + "*"):
        # Get image name
        img_name = os.path.basename(img)
        print("\n... processing: " + img_name)

        if img_name in assets_dict:
            # Rename image
            os.rename(assets_folder + img_name, assets_folder + assets_dict[img_name])
            print("... renamed: " + img_name + " to " + assets_dict[img_name])
        else:
            # Delete image
            os.remove(img)
            print("... not found: " + img_name)

print("... done!")
        