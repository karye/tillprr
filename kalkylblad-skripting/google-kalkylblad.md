# Google kalkylblad

## Lära känna Google kalkylblad

I Google kalkylblad kan du skapa kalkylblad, liknande Excel. Du kan även skapa formulär, liknande Microsoft Forms. Du kan även skapa skript, liknande VBA i Excel. Detta gör att du kan skapa avancerade kalkylblad som kan användas av andra användare.

![](../.gitbook/assets/google-kalkylblad.png)

# Google Apps Script

Apps Script är ett skriptspråk som används för att skapa skript i Google kalkylblad, Google Dokument, Google Formulär och Google Presentationer. Det är ett JavaScript-baserat språk som är enkelt att lära sig. 

![](../.gitbook/assets/apps-script-1.png)