---
description: Introduktion till kursen tillämpad programmering
---

# Introduktion

## **Centralt innehåll**

Undervisningen i kursen ska behandla följande centrala innehåll:

* **Begrepp, teorier, modeller och metoder inom programmering**.
* Förmåga att **lösa problem** i en datavetenskaplig kontext.
* Tillämpningar inom valt kunskapsområde, till exempel **industriell programmering**, spelprogrammering, utveckling av administrativa system eller pedagogiska program, applikationsutveckling eller som hjälpmedel för att skapa eller bearbeta bild, ljud och film.
* Hur tillämpad programmering kan användas som verktyg i behandlingen av omfångsrika problemsituationer. Programmeringens möjligheter och begränsningar i dessa situationer.
* Konsekvenser av tillämpad programmering för det valda kunskapsområdet.

## Raspberry Pi Pico

![Labb - blinkande led](<.gitbook/assets/image (6).png>)

![Picos kontakter och deras betydelse](<.gitbook/assets/image (4).png>)
