# Dialoger i RPG Maker

## Övning 1: En enkel dialog

### Steg 1: Skapa en NPC med en enkel hälsningsdialog

1. Öppna RPG Maker VX Ace Lite.
2. Välj **Events** från verktygsfältet.
3. Klicka på kartan där du vill placera NPC:n och högerklicka. Välj **New Event**.
4. Under **Graphic**, klicka på den lilla rutan för att välja en bild för NPC:n (t.ex. en bybo).
5. I **Event Commands**-fönstret, välj **Show Text**.
6. I textfönstret som dyker upp, skriv en enkel hälsning, till exempel: "Hej! Välkommen till vår by!".
7. Klicka på **OK** för att spara texten.
8. Klicka på **OK** igen för att spara händelsen.

Nu har du skapat en NPC som säger "Hej! Välkommen till vår by!" när spelaren interagerar med den.

## Övning 2: En dialog med val

### Steg 1: Skapa en NPC som frågar en fråga med valmöjligheter

1. Följ steg 1-4 från övning 1 för att skapa en ny NPC.
2. I **Event Commands**-fönstret, välj **Show Text** och skriv: "Hej! Vill du höra en hemlighet?".
3. Klicka på **OK** för att spara texten.
4. Lägg till ett nytt kommando genom att välja **Show Choices**.
5. I fönstret som dyker upp, skriv alternativen "Ja" och "Nej".
6. Klicka på **OK** för att spara valmöjligheterna.

### Steg 2: Hantera spelarens val

1. Under "When [Ja]", lägg till **Show Text** och skriv: "Okej, här är hemligheten: Det finns en skatt gömd bakom det stora trädet!".
2. Under "When [Nej]", lägg till **Show Text** och skriv: "Okej, kanske en annan gång då.".
3. Klicka på **OK** för att spara händelsen.

Nu har du skapat en NPC som frågar spelaren om de vill höra en hemlighet och reagerar olika beroende på spelarens svar.

## Övning 3: En flerdelad dialog

### Steg 1: Skapa en NPC med en flerdelad dialog

1. Följ steg 1-4 från övning 1 för att skapa en ny NPC.
2. I **Event Commands**-fönstret, välj **Show Text** och skriv: "Hej, jag har något viktigt att berätta.".
3. Klicka på **OK** för att spara texten.
4. Lägg till ett nytt **Show Text**-kommando och skriv: "Det handlar om den försvunna skatten.".
5. Klicka på **OK** för att spara texten.
6. Lägg till ett sista **Show Text**-kommando och skriv: "Du måste hitta den innan någon annan gör det.".
7. Klicka på **OK** för att spara texten.

### Steg 2: Lägg till väntetid mellan textdelarna

1. För att göra dialogen mer naturlig, kan du lägga till en kort väntetid mellan varje textdel.
2. Efter varje **Show Text**-kommando, lägg till ett **Wait**-kommando (finns under **Event Commands** > **Flow Control** > **Wait...**).
3. Ange väntetiden till cirka 30 frames (0.5 sekunder).
4. Klicka på **OK** för att spara varje väntetid.

Nu har du skapat en NPC med en flerdelad dialog som ger spelaren information i steg och pausar mellan varje del.

Absolut! Här är tre mer komplexa dialogövningar för RPG Maker VX Ace Lite som hjälper nybörjare att avancera sina färdigheter.

### Övning 4: En dialog med förutsättningar

#### Steg 1: Skapa en NPC som kontrollerar en switch

1. Skapa en ny NPC genom att följa steg 1-4 från övning 1.
2. I **Event Commands**-fönstret, välj **Show Text** och skriv: "Hej! Har du redan pratat med borgmästaren?".
3. Lägg till **Show Choices** med alternativen "Ja" och "Nej".
4. Under "When [Ja]", lägg till **Show Text** och skriv: "Borgmästaren är klok. Lycka till!".
5. Under "When [Nej]", lägg till **Show Text** och skriv: "Du borde prata med borgmästaren först.".

#### Steg 2: Skapa en händelse för borgmästaren

1. Skapa en ny händelse för borgmästaren någon annanstans på kartan.
2. Under **Graphic**, välj en bild för borgmästaren.
3. I **Event Commands**-fönstret, välj **Show Text** och skriv: "Välkommen, jag har något viktigt att berätta.".
4. Lägg till **Control Switches** och aktivera en switch, till exempel "PratatMedBorgmästaren".

#### Steg 3: Lägg till förutsättning till NPC

1. Gå tillbaka till den ursprungliga NPC:n och skapa en ny sida i händelsen.
2. Ställ in förutsättningen till switchen "PratatMedBorgmästaren".
3. I **Event Commands**, välj **Show Text** och skriv: "Tack för att du pratat med borgmästaren! Här är din belöning.".
4. Ge spelaren en belöning genom att lägga till **Change Items** eller **Change Gold**.

### Övning 5: En dialog som ändras över tid

#### Steg 1: Skapa en NPC med flera dialoger

1. Skapa en ny NPC genom att följa steg 1-4 från övning 1.
2. I **Event Commands**-fönstret, välj **Show Text** och skriv: "Hej! Hur mår du idag?".
3. Lägg till **Control Variables** och skapa en variabel, till exempel "DialogSteg", och sätt värdet till 1.

#### Steg 2: Skapa flera dialogsteg

1. Skapa en ny sida i NPC-händelsen.
2. Ställ in förutsättningen till att variabeln "DialogSteg" är lika med 1.
3. I **Event Commands**, välj **Show Text** och skriv: "Jag har hört att det ska bli fint väder.".
4. Lägg till **Control Variables** och öka "DialogSteg" med 1.
5. Upprepa processen för fler steg genom att skapa nya sidor och öka variabeln varje gång, med nya dialoger för varje steg.

### Övning 6: En dialog med gåtor och svar

#### Steg 1: Skapa en NPC som ger en gåta

1. Skapa en ny NPC genom att följa steg 1-4 från övning 1.
2. I **Event Commands**-fönstret, välj **Show Text** och skriv: "Jag har en gåta åt dig. Är du redo?".
3. Lägg till **Show Choices** med alternativen "Ja" och "Nej".

#### Steg 2: Lägg till gåtan

1. Under "When [Ja]", välj **Show Text** och skriv: "Vad går upp och ner men stannar alltid på samma plats?".
2. Lägg till **Input Number** och sätt en variabel, till exempel "GåtaSvar", med max siffror till 1.
3. Lägg till ett **Conditional Branch** för att kontrollera spelarens svar.
4. Om svaret är korrekt (t.ex. 1 för 'trappa'), lägg till **Show Text** och skriv: "Rätt svar! Här är din belöning.".
5. Ge spelaren en belöning genom att lägga till **Change Items** eller **Change Gold**.
6. Om svaret är fel, lägg till **Show Text** och skriv: "Fel svar, försök igen senare.".

#### Steg 3: Hantera felaktiga svar

1. Lägg till en ny sida som aktiveras när spelaren svarat fel (baserat på variabeln "GåtaSvar").
2. I **Event Commands**, välj **Show Text** och skriv: "Vill du försöka igen?".
3. Lägg till **Show Choices** med alternativen "Ja" och "Nej".
4. Under "When [Ja]", återställ variabeln "GåtaSvar" och gå tillbaka till gåtan.

