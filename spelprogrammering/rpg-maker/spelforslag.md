﻿# Förslag på enkla spel

Dessa idéer bör ge en bra grund att börja från och de involverar olika tekniker och element som är möjliga att implementera i RPG Maker VX Ace.

## 1. Skattjakten
**Koncept**: Spelaren måste hitta en dold skatt i en labyrintisk grotta.
- **Kartor**: En by, en skog och en grotta.
- **Transfer**: Från byn till skogen, och från skogen till grottan.
- **Dörrar**: Hemliga dörrar i grottan som kräver lösenord eller nycklar.
- **Variabler**: Antal nycklar samlade, lösenord funnet, etc.

## 2. Tidsresenären
**Koncept**: Spelaren reser mellan olika tidsepoker för att lösa pussel.
- **Kartor**: Samma plats men i olika tidsepoker (t.ex. medeltiden, nutid, framtid).
- **Transfer**: Tidsmaskiner eller portaler för att resa mellan tiderna.
- **Dörrar**: Dörrar som endast kan öppnas i vissa tidsepoker.
- **Variabler**: Tidsepok, antal pussel lösta.

## 3. Rädda Byborna
**Koncept**: Spelaren måste rädda byborna från en invasion av monster.
- **Kartor**: By, skog, och monstrets gömställe.
- **Transfer**: Från byn till skogen, och sedan till monstrets gömställe.
- **Dörrar**: Dörrar i byn som leder till butiker, och dörrar i gömstället som kräver kod.
- **Variabler**: Antal räddade bybor, antal besegrade monster.

## 4. De Fyra Elementen
**Koncept**: Samla de fyra elementens kraft för att besegra den onda trollkarlen.
- **Kartor**: Fyra olika tempel (eld, vatten, jord, luft) och en ond borg.
- **Transfer**: Från varje tempel till den onda borgen.
- **Dörrar**: Varje tempel har dörrar som endast kan öppnas genom att lösa tempelspecifika gåtor.
- **Variabler**: Element samlade, gåtor lösta.

## 5. Skolmysteriet
**Koncept**: Lösa ett mysterium som äger rum i en skola.
- **Kartor**: Klassrum, korridorer, skolgård, och ett hemligt rum.
- **Transfer**: Mellan olika delar av skolan.
- **Dörrar**: Låsta dörrar som kräver nycklar eller lösenord.
- **Variabler**: Antal ledtrådar funna, misstänkta intervjuade.
