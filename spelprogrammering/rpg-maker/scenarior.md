# Scenarior i RPG Maker

## Scenario 1: En magisk portal som teleporterar spelaren

### Steg 1: Skapa portalen

1. Gå till **Events**.
2. Välj en plats på kartan där du vill placera portalen.
3. Högerklicka och välj **New Event**.
4. Under **Graphic**, välj en bild för portalen.
5. I **Event Commands**, välj **Show Text** och skriv "Du steg in i portalen och teleporterades till en ny plats.".

### Steg 2: Lägg till teleporteringen

1. I samma händelse, välj **Transfer Player** under **Event Commands**.
2. Välj den nya platsen på kartan där spelaren ska teleporteras.
3. Klicka på **OK** för att spara händelsen.

## Scenario 2: En NPC som ger ett uppdrag

### Steg 1: Skapa NPC

1. Gå till **Events**.
2. Välj en plats på kartan där du vill placera NPC:n.
3. Högerklicka och välj **New Event**.
4. Under **Graphic**, välj en bild för NPC:n.
5. I **Event Commands**, välj **Show Text** och skriv "Hej äventyrare! Kan du hitta min förlorade amulett?".

### Steg 2: Lägg till val

1. Lägg till **Show Choices** under **Event Commands** med alternativen "Ja" och "Nej".
2. Under "When [Ja]", lägg till **Show Text** och skriv "Tack! Kom tillbaka när du har hittat den.".
3. Aktivera en switch, till exempel "UppdragPåbörjat".

### Steg 3: Skapa händelsen för amuletten

1. Gå till en annan plats på kartan och skapa en ny händelse som representerar den förlorade amuletten.
2. Under **Event Commands**, välj **Control Switches** och aktivera en switch, till exempel "AmulettHittad".
3. Lägg till **Show Text** och skriv "Du hittade amuletten!".

### Steg 4: Återvänd till NPC

1. Gå tillbaka till NPC:n och skapa en ny sida i händelsen.
2. Ställ in villkoret för den nya sidan till "AmulettHittad".
3. Under **Event Commands**, välj **Show Text** och skriv "Tack för att du hittade amuletten! Här är en belöning.".
4. Ge spelaren en belöning genom att lägga till **Change Items** eller **Change Gold**.

## Scenario 3: En dörr som kräver en nyckel

### Steg 1: Skapa dörren

1. Gå till **Events**.
2. Välj en plats på kartan där du vill placera dörren.
3. Högerklicka och välj **New Event**.
4. Under **Graphic**, välj en bild för dörren.
5. I **Event Commands**, välj **Show Text** och skriv "Dörren är låst. Du behöver en nyckel för att öppna den.".

### Steg 2: Skapa nyckeln

1. Gå till en annan plats på kartan och skapa en ny händelse som representerar nyckeln.
2. Under **Graphic**, välj en bild för nyckeln.
3. I **Event Commands**, välj **Show Text** och skriv "Du hittade en nyckel!".
4. Lägg till **Change Items** och ge spelaren en nyckel.

### Steg 3: Lägg till villkor för dörren

1. Gå tillbaka till dörrhändelsen och skapa en ny sida.
2. Ställ in villkoret för den nya sidan till att spelaren har nyckeln (kontrollera att spelaren har nyckeln i sitt inventory).
3. Under **Event Commands**, välj **Show Text** och skriv "Du använder nyckeln för att öppna dörren.".
4. Lägg till **Control Self Switch** och aktivera Self Switch A för att ändra dörrens tillstånd.
5. Skapa en ny sida och ställ in villkoret till Self Switch A. Ändra grafiken till en öppen dörr.
