# Skapa kartor

I den här tutorialen kommer vi att spel med en ö, en by och en stig som går mellan dem.  
I byn kommer det att finnas tre hus.

## Tutorial 1

### Skapa en spelvärld

Rita ut en ö med lite skog och lilte berg:

![](../../.gitbook/assets/rita-1.png)

## Tutorial 2

### Skapa en liten by

Byt till **Tileset B** och rita ut en liten by bestående av 2 rutor:

![](../../.gitbook/assets/rita-2.png)

Rita ut en stig som går från byn till bergen:

![](../../.gitbook/assets/rita-3.png)

### Skapa en ny karta

Skapa en till karta:

![](../../.gitbook/assets/rita-4.png)

Fyll den nya karta med gräs:

![](../../.gitbook/assets/rita-5.png)

Rita en ny stig som delar sig i tre:

![](../../.gitbook/assets/rita-6.png)

Ritan nu tre hus:

![](../../.gitbook/assets/rita-7.png)

.. och ett staket:

![](../../.gitbook/assets/rita-8.png)

### Skapa "transfer"

Spelaren ska kunna förflyttas sig från första kartan till den andra kartan. 

Välj **Events** och klicka på byn, högerklicka och välj **Quick Event Creation**:

Välj **Transfer** och direction **Up**:

![](../../.gitbook/assets/event-1.png)

Välj den andra kartan och början av stigen:

![](../../.gitbook/assets/event-2.png)

Men, spelaren ska även kunna gå tillbak till byn. Välj **Events** och klicka på stigen, högerklicka och välj **Quick Event Creation**:

![](../../.gitbook/assets/event-3.png)

För att göra det enklare att spela kopierar vi *eventet* till flera rutor i bottenraden:

![](../../.gitbook/assets/event-4.png)

