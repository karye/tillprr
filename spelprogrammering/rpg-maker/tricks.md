# Spelidé: Magiska Skogen – Rädda djuren!

Ett enkelt spel som fokuserar på att träna användning av variabler, switchar och villkor kan vara något i stil med en "frågesport" eller ett "pusselspel".

## Syfte
Att träna på att använda variabler, switchar och villkor genom att lösa pussel och svara på frågor för att rädda skogens djur.

## Kartor
- Byn (Startpunkt)
- Skogen
- Grottan
- Tempel

## Gameplay

1. **Byn:** Spelaren får i uppdrag att rädda skogens djur. För att göra detta behöver spelaren samla "Magiska Poäng" genom att lösa pussel och svara på frågor.

2. **Skogen:** Här finns olika djur som ställer frågor till spelaren. Varje rätt svar ger "Magiska Poäng".

3. **Grottan:** Spelaren måste lösa olika pussel för att komma vidare. Varje löst pussel aktiverar en switch som gör att spelaren kan gå vidare till nästa rum.

4. **Tempel:** Här använder spelaren de "Magiska Poäng" de samlat för att rädda djuren. Antalet poäng avgör spelets utfall.

## Exempel på Events och Kod

### Frågesport med Djur i Skogen

```plaintext
Event Name: FrågaFrånRäv
Trigger: Action Button

@>Show Text: "Vad äter jag helst?"
@>Show Choices: ["Kött", "Gräs", "Fisk"]
  : When ["Kött"]
    @>Show Text: "Rätt svar!"
    @>Control Variables: [0001: MagiskaPoäng] += 10
  : When ["Gräs"]
    @>Show Text: "Fel svar."
  : When ["Fisk"]
    @>Show Text: "Fel svar."
@>End
```

### Pussel i Grottan

```plaintext
Event Name: StenPussel
Trigger: Action Button

@>Conditional Branch: Variable [0002: StenarFlyttade] == 3
  @>Show Text: "Du har löst pusslet!"
  @>Control Switches: [0001: PusselLöst] = ON
  : Else
  @>Show Text: "Flytta de tre stenarna till rätt plats."
@>End
```

### Tempel Event

```plaintext
Event Name: RäddaDjuren
Trigger: Action Button

@>Conditional Branch: Variable [0001: MagiskaPoäng] >= 50
  @>Show Text: "Du har räddat djuren!"
  @>End Game
  : Else
  @>Show Text: "Du behöver fler Magiska Poäng."
@>End
```

Här använder vi variabeln `MagiskaPoäng` för att hålla koll på hur många poäng spelaren har, och switchen `PusselLöst` för att avgöra om ett specifikt pussel har lösts.
