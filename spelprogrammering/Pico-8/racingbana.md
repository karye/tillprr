# Racingbana i Pico-8

## Syfte

Syftet med denna laboration är att skapa en racingbana i Pico-8.

## Steg 1 - Rita en racingbana

Vi ritar en bil vänd åt höger, vänster, ned och upp och gräset och vägen:

![alt text](../../.gitbook/assets/image-11.png)

För att styra bilden använder vi BTN(0):

![alt text](../../.gitbook/assets/image-12.png)

## Steg 2 - Skapa en racingbana i Map-läge

Vi använder spriten för att rita ut en racingbana:

![alt text](../../.gitbook/assets/image-13.png)

## Steg 3 - Skapa en racingbana i Pico-8

Med MAP() ritas kartan med racingbanan ut:

![alt text](../../.gitbook/assets/image-14.png)

![alt text](../../.gitbook/assets/image-15.png)

## Steg 4 - Styra bilen

Lägg till alla knappar för att styra bilen på banan:

![alt text](../../.gitbook/assets/image-16.png)

## Steg 5 - Kollisioner

Skapa en funktion för kollisioner:
(Sprite 2 är gräset)

![alt text](../../.gitbook/assets/image-17.png)

## Steg 6 - Game Over

Använd GAMEOVER() funktion i DRAW():

![alt text](../../.gitbook/assets/image-18.png)

## Steg 7 - Poäng

Lägg till en ny variable P=100 för poäng i _INIT():

![alt text](../../.gitbook/assets/image-19.png)

Skriv ut poängen som “POINT …” i _DRAW():

![alt text](../../.gitbook/assets/image-20.png)

Minska med 1 poäng om bilen åker på gräset inuti IF-satsen i _DRAW():

![alt text](../../.gitbook/assets/image-21.png)

För att få ett riktigt avslut delar vi upp IF-satsen i _DRAW():

![alt text](../../.gitbook/assets/image-22.png)

Men vi kan fortfarande köra bilen så behöver en IF-sats till i _UPDATE():

![alt text](../../.gitbook/assets/image-23.png)

## Utmaningar

1. Hur göra så att bilen får pluspoäng?