# Spel i Pico-8

![Alt text](../../.gitbook/assets/PICO-8_logo.png)

Pico-8 är ett spelprogrammeringsspråk som är enkelt att komma igång med. Det är gratis och finns till Windows, Mac och Linux. Det finns även en webbversion som är begränsad till 30 dagar.

## Utvecklingsmiljön

Pico-8 finns för studenter på [Pico-8 edu](https://www.pico-8-edu.com)

## Lära sig Pico-8

I Pico-8 kan du skriva kod, rita sprites, skapa ljud och spela in musik. 

### Kodfönstret

I kodfönstret kan du skriva kod. Du kan även skriva kommentarer genom att skriva `--` följt av en kommentar.

![](../../.gitbook/assets/pico-1.png)

### Sprite-fönstret

I sprite-fönstret kan du rita sprites. Du kan även använda piltangenterna för att flytta på en pixel i taget. Du kan även använda `shift` + piltangenterna för att flytta på 8 pixlar i taget.

![](../../.gitbook/assets/pico-2.png)

### Ljudfönstret

I ljudfönstret kan du skapa ljud och spela in musik. Du kan även använda tangentbordet för att spela in musik. Du kan även använda `shift` + piltangenterna för att flytta på 8 pixlar i taget.

![](../../.gitbook/assets/pico-3.png)

### Skapa spel i Pico-8

Pico-8 har ett eget språk som är väldigt likt Lua. 

Grunderna till Pico-8 är att skapa en funktion som heter `_UPDATE` och en funktion som heter `_DRAW`. Funktionen `_UPDATE` körs 30 gånger per sekund och funktionen `_DRAW` körs 60 gånger per sekund.

Så här kan du skapa ett spel där en sprite rör sig från vänster till höger:

```lua
function _INIT()
    -- Initiera variabler
    x=0
    y=0
end

function _UPDATE()
    -- Uppdatera variabler
    x=x+1
end

function _DRAW()
    cls()
    -- Rita sprite 0 på position x,y
    spr(0,x,y)
end
```

### Lyssna på tangenttryckningar

I Pico-8 kan du lyssna på tangenttryckningar. Du kan använda funktionen `btn` för att lyssna på tangenttryckningar. Funktionen `btn` tar en parameter som är en siffra från 0 till 5. 0 är vänster, 1 är höger, 2 är uppåt, 3 är nedåt, 4 är knappen Z och 5 är knappen X.

![alt text](<../../.gitbook/assets/Skärmbild 2024-01-23 161257.png>)

För att lyssna på tangenttryckningar kan du använda funktionen `btn`:

```lua
function _INIT()
    -- Initiera variabler
    x=0
    y=0
end

function _UPDATE()
    -- Om tangent 0 är nedtryckt, dvs till vänster
    if btn(0) then
        x=x-1
    end
    -- Om tangent 1 är nedtryckt, dvs till höger
    if btn(1) then
        x=x+1
    end
    -- Om tangent 2 är nedtryckt, dvs uppåt
    if btn(2) then
        y=y-1
    end
    -- Om tangent 3 är nedtryckt, dvs nedåt
    if btn(3) then
        y=y+1
    end
end

function _DRAW()
    cls()
    -- Rita sprite 0 på position x,y
    spr(0,x,y)
end
```

## Resurser

[text](../../.gitbook/assets/RPi_PiPico_Digital_v10.pdf)