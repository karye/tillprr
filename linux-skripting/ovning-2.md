# Navigering i filsystemet med skript

Målet är att du ska bekanta dig med de grundläggande kommandona för att navigera i filsystemet och skapa skript som automatiserar dessa uppgifter. Du kommer att skapa flera skript som visar den aktuella platsen, listar filer och mappar, byter mappar, och navigerar uppåt i filsystemet.

> **Tips:**  
> - Använd terminalen (t.ex. WSL Ubuntu).  
> - Kom ihåg att spara dina filer (t.ex. med nano) och göra dem körbara med `chmod +x filnamn.sh`.

---

## Mål:
1. Visa aktuell plats med kommandot `pwd`.
2. Lista filer och mappar med kommandot `ls`.
3. Navigera mellan mappar med `cd` och `cd ..`.
4. Skapa skript som automatiserar dessa kommandon.
5. Utöka skripten med egna utmaningar och variationer.

---

## Steg 1: Navigera manuellt i terminalen
**Mål:** Bekanta dig med de grundläggande navigeringskommandona.

1. Öppna terminalen.
2. Skriv följande kommandon och observera vad som händer:
   - Visa aktuell plats:
     ```bash
     pwd
     ```
   - Lista filer och mappar i den aktuella mappen:
     ```bash
     ls
     ```
   - Byt till en annan mapp (t.ex. `Documents`):
     ```bash
     cd Documents
     ```
   
**Utmaning:** Prova att navigera till en annan mapp (t.ex. `Downloads`) och använd `pwd` för att se din nya plats.

---

## Steg 2: Skapa ett skript för att automatisera navigering
**Mål:** Skapa ett skript som skriver ut den aktuella platsen och listar innehållet.

1. Gå tillbaka till hemkatalogen:
   ```bash
   cd ~
   ```
2. Skapa ett nytt skript med namnet `navigera.sh`:
   ```bash
   nano navigera.sh
   ```

---

## Steg 3: Lägg till kommandon i skriptet
**Mål:** Skriv in kommandon i skriptet som visar aktuell plats och listar filer.

1. I `navigera.sh` skriver du följande kod:
   ```bash
   #!/bin/bash
   echo "Din aktuella plats är:"
   pwd

   echo "Här är filerna och mapparna:"
   ls
   ```
2. **Förklaring:**  
   - `pwd` visar var du befinner dig just nu.  
   - `ls` listar allt som finns i den aktuella mappen.  
   - `echo` används för att skriva ut tydlig information innan resultaten visas.

---

## Steg 4: Gör skriptet körbart och kör det
**Mål:** Lär dig att göra ett skript körbart och sedan köra det.

1. Spara filen i nano med `CTRL+O` (Enter) och avsluta med `CTRL+X`.
2. Gör skriptet körbart:
   ```bash
   chmod +x navigera.sh
   ```
3. Kör skriptet:
   ```bash
   ./navigera.sh
   ```
4. **Utmaning:** Testa att köra skriptet igen efter att du har ändrat några filer i mappen (lägg till en fil eller mapp) för att se uppdaterad information.

---

## Steg 5: Utöka skriptet med att lista en specifik mapp
**Mål:** Lägg till ett kommando i skriptet för att lista innehållet i en specifik mapp, t.ex. `Documents`.

1. Öppna `navigera.sh` igen:
   ```bash
   nano navigera.sh
   ```
2. Lägg till följande rader i slutet av filen:
   ```bash
   echo "Innehåll i mappen Dokument:"
   ls ~/Documents
   ```
3. Spara, avsluta, och kör skriptet:
   ```bash
   ./navigera.sh
   ```
4. **Förklaring:**  
   - `ls ~/Documents` listar innehållet i mappen `Documents` i din hemkatalog.

**Utmaning:** Ändra sökvägen så att den visar innehållet i en annan mapp (t.ex. `Downloads`).

---

## Steg 6: Navigera till en annan mapp och visa plats
**Mål:** Skapa ett skript som navigerar till en annan mapp med `cd` och visar den nya platsen.

1. Skapa ett nytt skript med namnet `bytmapp.sh`:
   ```bash
   nano bytmapp.sh
   ```
2. Skriv in följande kod:
   ```bash
   #!/bin/bash
   echo "Du befinner dig nu i:"
   pwd

   echo "Byter nu mapp till /tmp..."
   cd /tmp

   echo "Din nya plats är:"
   pwd
   ```
3. Spara, avsluta, gör skriptet körbart:
   ```bash
   chmod +x bytmapp.sh
   ./bytmapp.sh
   ```
4. **Förklaring:**  
   - Kommandot `cd /tmp` byter mapp till `/tmp`.  
   - Efter bytet skrivs den nya platsen ut med `pwd`.

**Utmaning:** Ändra skriptet så att det byter till en mapp du ofta använder (t.ex. din hemkatalog med `cd ~`).

---

## Steg 7: Navigera uppåt i filsystemet med `cd ..`
**Mål:** Visa hur du kan gå upp en nivå i mappstrukturen.

1. Skapa ett skript med namnet `uppat.sh`:
   ```bash
   nano uppat.sh
   ```
2. Skriv in följande kod:
   ```bash
   #!/bin/bash
   echo "Startplats:"
   pwd

   echo "Navigerar upp en nivå..."
   cd ..

   echo "Ny plats:"
   pwd
   ```
3. Spara, avsluta, gör körbart och kör skriptet:
   ```bash
   chmod +x uppat.sh
   ./uppat.sh
   ```
4. **Förklaring:**  
   - `cd ..` flyttar dig upp en mapp i filsystemet.  
   - Genom att skriva ut platsen före och efter kan du se skillnaden.

**Utmaning:** Kör skriptet från en djupare mapp (t.ex. en undermapp i Documents) och observera hur platsen ändras.

---

## Steg 8: Kombinera flera navigeringskommandon i ett skript
**Mål:** Skapa ett skript som utför en sekvens av navigeringskommandon för att ge en överblick av hur du rör dig i filsystemet.

1. Skapa ett skript med namnet `navigator.sh`:
   ```bash
   nano navigator.sh
   ```
2. Skriv in följande kod:
   ```bash
   #!/bin/bash
   echo "Startar i hemkatalogen:"
   cd ~
   pwd

   echo "Listar innehållet i hemkatalogen:"
   ls

   echo "Navigerar till mappen Documents..."
   cd Documents
   pwd

   echo "Listar innehållet i Documents:"
   ls

   echo "Navigerar tillbaka till hemkatalogen..."
   cd ~
   pwd
   ```
3. Spara, avsluta, gör skriptet körbart och kör det:
   ```bash
   chmod +x navigator.sh
   ./navigator.sh
   ```
4. **Förklaring:**  
   - Skriptet visar hur du kan byta mellan mappar och använda `pwd` samt `ls` för att verifiera var du befinner dig och vad som finns i varje mapp.

**Utmaning:** Lägg till en rad som navigerar till en annan mapp (t.ex. `Downloads`) och listar innehållet där.

---

## Steg 9: Utmaning – Navigera till en mapp med ett argument
**Mål:** Skapa ett skript som tar emot ett mappsnamn som argument och navigerar till den mappen.

1. Skapa ett skript med namnet `go_to.sh`:
   ```bash
   nano go_to.sh
   ```
2. Skriv in följande kod:
   ```bash
   #!/bin/bash
   echo "Försöker navigera till mappen: $1"
   cd $1
   echo "Nu befinner du dig i:"
   pwd

   echo "Innehållet i mappen $1:"
   ls
   ```
3. Spara, avsluta, gör körbart och testa med:
   ```bash
   chmod +x go_to.sh
   ./go_to.sh ~/Documents
   ```
4. **Förklaring:**  
   - Skriptet använder `$1` för att ta emot det första argumentet (mappsökvägen) och navigerar dit med `cd $1`.

**Utmaning:** Testa att köra skriptet med olika mappsökvägar och se hur utskriften ändras.

---

## Steg 10: Utmaning – Skapa en navigationsmeny
**Mål:** Skapa ett skript som visar en enkel navigationsmeny med alternativ (endast som visning).

1. Skapa ett skript med namnet `menu.sh`:
   ```bash
   nano menu.sh
   ```
2. Skriv in följande kod:
   ```bash
   #!/bin/bash
   echo "=== Navigationsmeny ==="
   echo "1. Visa aktuell plats"
   echo "2. Lista filer i hemkatalogen"
   echo "3. Navigera till Documents och lista innehållet"
   echo "4. Navigera upp en nivå"
   echo "5. Navigera till en specifik mapp (ange sökväg som argument vid körning)"
   echo "======================="
   ```
3. Spara, avsluta, gör körbart och kör skriptet:
   ```bash
   chmod +x menu.sh
   ./menu.sh
   ```
4. **Förklaring:**  
   - Detta skript visar en statisk meny med olika navigeringsalternativ. Även om det inte tar emot input, kan du senare utöka det med interaktivitet.
   
**Utmaning:** Fundera på hur du skulle kunna göra menyn interaktiv genom att läsa in ett val från användaren (t.ex. med `read`) och sedan köra motsvarande kommandon – detta kan vara ett steg för framtida övningar.

