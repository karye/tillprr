# Vad är Linux?

## Grunder - Linux kommandon

{% embed url="https://youtu.be/2PGnYjbYuUo" %}

## Linuxböcker

### Linuxboken
Linuxboken har några år på nacken. Allt som rör terminalen är fortfarande aktuellt.  
Däremot är den grafiska delen förlegad.  

* [linuxboken.pdf](http://www.rejas.se/docs/linuxboken.pdf)

### Linuxcommand
Linuxcommand är en bok helt inriktad på arbetet i terminalen.  

* [linuxcommand.org](http://linuxcommand.org/lc3_wss0010.php)

## En praktisk introduktion till Ubuntu



