# Skript med argument – hantera flera argument   

Målet med dessa övningar är att skapa skript som kan hantera flera argument. Du kommer att lära dig att kontrollera antalet argument, iterera genom dem och utföra olika operationer beroende på deras värden.

> **Tips innan du börjar:**  
> - Öppna terminalen (t.ex. med WSL Ubuntu) och gå till hemkatalogen med:  
>   ```bash
>   cd ~
>   ```  
> - Använd en textredigerare (t.ex. `nano`) för att skapa och redigera dina skript.  
> - Kom ihåg att spara filen, göra den körbar med:  
>   ```bash
>   chmod +x skriptets_namn.sh
>   ```  
> - Kör skriptet med:  
>   ```bash
>   ./skriptets_namn.sh
>   ```

---

## Övning 8.1: Enkel argumentkontroll och utskrift

**Mål:**  
- Lära sig att ta emot argument och skriva ut antalet samt varje argument.

**Steg för steg:**
1. Öppna terminalen och gå till hemkatalogen:
   ```bash
   cd ~
   ```
2. Skapa en ny fil med namnet `lista_namn.sh`:
   ```bash
   nano lista_namn.sh
   ```
3. Skriv in följande kod:
   ```bash
   #!/bin/bash

   # Kontrollera att minst ett argument har skickats in
   if [ $# -eq 0 ]; then
       echo "Du måste ange minst ett namn!"
       exit 1
   fi

   # Skriv ut antalet argument
   echo "Du angav $# namn."

   # Loopar igenom alla argument och skriver ut dem
   for namn in "$@"
   do
       echo "Hej, $namn!"
   done
   ```
4. Spara, gör körbar med:
   ```bash
   chmod +x lista_namn.sh
   ```
5. Testa skriptet:
   ```bash
   ./lista_namn.sh Alice Bob Charlie
   ```

**Förklaring:**  
- `$#` anger antalet argument.  
- `"$@"` innehåller alla argument.  
- If-satsen kontrollerar att minst ett argument finns.

**Utmaning:** Prova att köra skriptet utan argument och se att meddelandet visas.

---

## Övning 8.2: Numrera varje argument med en räknare

**Mål:**  
- Skriva ut varje argument med ett löpnummer.

**Steg för steg:**
1. Skapa en fil med namnet `numrera_namn.sh`:
   ```bash
   nano numrera_namn.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash

   if [ $# -eq 0 ]; then
       echo "Ange minst ett namn!"
       exit 1
   fi

   echo "Du angav $# namn."

   i=1
   for namn in "$@"
   do
       echo "$i. Hej, $namn!"
       ((i++))
   done
   ```
3. Spara, gör körbar och testa:
   ```bash
   chmod +x numrera_namn.sh
   ./numrera_namn.sh Anna Erik Lisa
   ```

**Förklaring:**  
- Variabeln `i` fungerar som en räknare som ökas med varje iteration.

**Utmaning:** Ändra meddelandet så att det istället säger "Namnet #i: $namn".

---

## Övning 8.3: Använd "$*" istället för "$@"

**Mål:**  
- Visa skillnaden mellan "$*" och "$@".

**Steg för steg:**
1. Skapa en fil med namnet `arg_stars.sh`:
   ```bash
   nano arg_stars.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Med \$*:"
   for arg in "$*"
   do
       echo "$arg"
   done

   echo "Med \$@:"
   for arg in "$@"
   do
       echo "$arg"
   done
   ```
3. Spara, gör körbar och testa:
   ```bash
   chmod +x arg_stars.sh
   ./arg_stars.sh "Anna Maria" Erik
   ```

**Förklaring:**  
- Med "$*" betraktas alla argument som en enda sträng.  
- Med "$@" behandlas varje argument separat.

**Utmaning:** Lägg till fler argument med mellanslag och observera skillnaden.

---

## Övning 8.4: Kontrollera antalet argument

**Mål:**  
- Visa ett meddelande om antalet argument inte motsvarar förväntat antal.

**Steg för steg:**
1. Skapa en fil med namnet `krav_antal.sh`:
   ```bash
   nano krav_antal.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash

   if [ $# -ne 2 ]; then
       echo "Du måste ange exakt 2 argument!"
       exit 1
   fi

   echo "Du angav 2 argument: $1 och $2."
   ```
3. Spara, gör körbar och testa:
   ```bash
   chmod +x krav_antal.sh
   ./krav_antal.sh Foo Bar
   ./krav_antal.sh Foo
   ```

**Förklaring:**  
- `-ne` står för "not equal", och skriptet avslutas om antalet argument inte är 2.

**Utmaning:** Ändra så att det krävs exakt 3 argument.

---

## Övning 8.5: Skript som hälsar på varje namn med ett personligt meddelande

**Mål:**  
- Skriv ut en personlig hälsning med argumenten.

**Steg för steg:**
1. Skapa en fil med namnet `personliga_halsningar.sh`:
   ```bash
   nano personliga_halsningar.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   if [ $# -eq 0 ]; then
       echo "Ange minst ett namn!"
       exit 1
   fi

   for namn in "$@"
   do
       echo "Hej, $namn! Hoppas du har en fantastisk dag!"
   done
   ```
3. Spara, gör körbar och kör skriptet:
   ```bash
   chmod +x personliga_halsningar.sh
   ./personliga_halsningar.sh Sara Johan
   ```

**Förklaring:**  
- Varje argument används för att ge en personlig hälsning.

**Utmaning:** Lägg till en extra fråga som "Hur mår du, $namn?" efter hälsningen.

---

## Övning 8.6: Omvandla alla argument till stora bokstäver

**Mål:**  
- Visa hur man kan omvandla text till versaler innan utskrift.

**Steg för steg:**
1. Skapa en fil med namnet `stora_bokstaver.sh`:
   ```bash
   nano stora_bokstaver.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   for arg in "$@"
   do
       # Omvandla till stora bokstäver med tr
       stora=$(echo "$arg" | tr '[:lower:]' '[:upper:]')
       echo "Hej, $stora!"
   done
   ```
3. Spara, gör körbar och kör:
   ```bash
   chmod +x stora_bokstaver.sh
   ./stora_bokstaver.sh alice bob
   ```

**Förklaring:**  
- `tr '[:lower:]' '[:upper:]'` omvandlar alla små bokstäver till stora.

**Utmaning:** Försök att också omvandla till gemener (små bokstäver) med `tr '[:upper:]' '[:lower:]'`.

---

## Övning 8.7: Skriv ut argumenten i omvänd ordning

**Mål:**  
- Visa hur du kan iterera genom argumenten baklänges.

**Steg för steg:**
1. Skapa en fil med namnet `omvandla_ordning.sh`:
   ```bash
   nano omvandla_ordning.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Argumenten i omvänd ordning:"
   for (( idx=$#; idx>0; idx-- ))
   do
       eval "arg=\$$idx"
       echo "$arg"
   done
   ```
3. Spara, gör körbar och testa:
   ```bash
   chmod +x omvandla_ordning.sh
   ./omvandla_ordning.sh Ett Två Tre Fyra
   ```

**Förklaring:**  
- En C-liknande for-loop används för att iterera baklänges genom argumenten.

**Utmaning:** Ändra skriptet så att det skriver ut varje argument med dess ursprungliga index.

---

## Övning 8.8: Summera numeriska argument

**Mål:**  
- Ta emot ett antal numeriska argument och beräkna deras summa.

**Steg för steg:**
1. Skapa en fil med namnet `summera.sh`:
   ```bash
   nano summera.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   summa=0
   for tal in "$@"
   do
       summa=$((summa + tal))
   done
   echo "Summan av argumenten är: $summa"
   ```
3. Spara, gör körbar och kör:
   ```bash
   chmod +x summera.sh
   ./summera.sh 5 10 15
   ```

**Förklaring:**  
- Varje numeriskt argument läggs till variabeln `summa` med hjälp av aritmetisk expansion.

**Utmaning:** Lägg till en kontroll som säkerställer att alla argument verkligen är siffror.

---

## Övning 8.9: Beräkna medelvärdet av numeriska argument

**Mål:**  
- Summera talen och dividera med antalet argument.

**Steg för steg:**
1. Skapa en fil med namnet `medelvärde.sh`:
   ```bash
   nano medelvarde.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   summa=0
   antal=$#
   for tal in "$@"
   do
       summa=$((summa + tal))
   done
   medel=$(echo "scale=2; $summa / $antal" | bc)
   echo "Medelvärdet av argumenten är: $medel"
   ```
3. Spara, gör körbar och kör:
   ```bash
   chmod +x medelvarde.sh
   ./medelvarde.sh 10 20 30 40
   ```

**Förklaring:**  
- `bc` används för att utföra decimaldivision med angiven precision.

**Utmaning:** Prova med olika antal tal och ändra precisionen (t.ex. scale=3).

---

## Övning 8.10: Hitta det största talet bland argumenten

**Mål:**  
- Jämför argumenten för att hitta det största värdet.

**Steg för steg:**
1. Skapa en fil med namnet `storst.sh`:
   ```bash
   nano storst.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   if [ $# -eq 0 ]; then
       echo "Ange minst ett tal!"
       exit 1
   fi

   max=$1
   for tal in "$@"
   do
       if [ "$tal" -gt "$max" ]; then
           max=$tal
       fi
   done
   echo "Det största talet är: $max"
   ```
3. Spara, gör körbar och kör:
   ```bash
   chmod +x storst.sh
   ./storst.sh 5 12 3 9
   ```

**Förklaring:**  
- Skriptet jämför varje tal med det nuvarande största och uppdaterar variabeln om det hittas ett större tal.

**Utmaning:** Ändra skriptet för att hitta det minsta talet istället.

---

## Övning 8.11: Hitta det minsta talet bland argumenten

**Mål:**  
- Jämför argumenten för att hitta det minsta värdet.

**Steg för steg:**
1. Skapa en fil med namnet `minst.sh`:
   ```bash
   nano minst.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   if [ $# -eq 0 ]; then
       echo "Ange minst ett tal!"
       exit 1
   fi

   min=$1
   for tal in "$@"
   do
       if [ "$tal" -lt "$min" ]; then
           min=$tal
       fi
   done
   echo "Det minsta talet är: $min"
   ```
3. Spara, gör körbar och kör:
   ```bash
   chmod +x minst.sh
   ./minst.sh 8 3 10 2 15
   ```

**Förklaring:**  
- Liknande som i övning 8.10, men med `-lt` (mindre än) för att hitta det lägsta värdet.

**Utmaning:** Kombinera övningarna för att visa både det största och minsta talet.

---

## Övning 8.12: Skriv ut varje argument med sitt index

**Mål:**  
- Skriv ut varje argument tillsammans med dess position i listan.

**Steg för steg:**
1. Skapa en fil med namnet `indexera.sh`:
   ```bash
   nano indexera.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   i=1
   for arg in "$@"
   do
       echo "Argument $i: $arg"
       ((i++))
   done
   ```
3. Spara, gör körbar och kör:
   ```bash
   chmod +x indexera.sh
   ./indexera.sh a b c d
   ```

**Förklaring:**  
- En räknare `i` används för att visa index tillsammans med varje argument.

**Utmaning:** Lägg till en rubrik som "Lista över argument:" innan loopen.

---

## Övning 8.13: Kombinera alla argument till en mening

**Mål:**  
- Sätt ihop alla argument till en lång mening.

**Steg för steg:**
1. Skapa en fil med namnet `sammanstallning.sh`:
   ```bash
   nano sammanstallning.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   mening="$*"
   echo "Din mening är: $mening"
   ```
3. Spara, gör körbar och kör:
   ```bash
   chmod +x sammanstallning.sh
   ./sammanstallning.sh Det här är en lång mening gjord av flera argument.
   ```

**Förklaring:**  
- Variabeln `"$*"` innehåller alla argument som en enda sträng.

**Utmaning:** Försök att använda `"$@"` och jämför skillnaden.

---

## Övning 8.14: Kontrollera om ett specifikt argument finns

**Mål:**  
- Sök efter ett visst ord bland argumenten och skriv ut ett meddelande om det finns.

**Steg för steg:**
1. Skapa en fil med namnet `sok_ord.sh`:
   ```bash
   nano sok_ord.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   ord="Banana"
   hittad=0

   for arg in "$@"
   do
       if [ "$arg" = "$ord" ]; then
           hittad=1
       fi
   done

   if [ $hittad -eq 1 ]; then
       echo "Ordet '$ord' finns bland argumenten!"
   else
       echo "Ordet '$ord' hittades inte."
   fi
   ```
3. Spara, gör körbar och kör:
   ```bash
   chmod +x sok_ord.sh
   ./sok_ord.sh Apple Orange Banana Grape
   ```

**Förklaring:**  
- Skriptet letar efter ett specifikt ord och sätter en flagga om det hittas.

**Utmaning:** Låt användaren ange ordet att söka efter istället för att ha det hårdkodat.

---

## Övning 8.15: Omvandla argumenten till gemener (små bokstäver)

**Mål:**  
- Visa hur man kan konvertera varje argument till små bokstäver.

**Steg för steg:**
1. Skapa en fil med namnet `gemener.sh`:
   ```bash
   nano gemener.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   for arg in "$@"
   do
       gemener=$(echo "$arg" | tr '[:upper:]' '[:lower:]')
       echo "Hej, $gemener!"
   done
   ```
3. Spara, gör körbar och kör:
   ```bash
   chmod +x gemener.sh
   ./gemener.sh ALICE BOB
   ```

**Förklaring:**  
- `tr '[:upper:]' '[:lower:]'` omvandlar text från versaler till gemener.

**Utmaning:** Kombinera med övning 8.6 för att jämföra båda resultaten.

---

## Övning 8.16: Skriv ut argumenten sorterade alfabetiskt

**Mål:**  
- Sortera argumenten innan utskrift.

**Steg för steg:**
1. Skapa en fil med namnet `sortera.sh`:
   ```bash
   nano sortera.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   # Spara alla argument i en variabel
   alla="$@"
   echo "Argumenten före sortering: $alla"

   # Använd kommandot sort för att sortera argumenten (varje argument på en rad)
   for arg in $(echo "$alla" | tr ' ' '\n' | sort)
   do
       echo "Sorterade: $arg"
   done
   ```
3. Spara, gör körbar och kör:
   ```bash
   chmod +x sortera.sh
   ./sortera.sh Zebra Apelsin Banan
   ```

**Förklaring:**  
- Vi använder `tr` för att byta ut mellanslag mot radbrytningar och sedan `sort`.

**Utmaning:** Prova att sortera argumenten i omvänd ordning med `sort -r`.

---

## Övning 8.17: Skriv ut det första och sista argumentet separat

**Mål:**  
- Visa hur du hämtar och skriver ut det första och sista argumentet.

**Steg för steg:**
1. Skapa en fil med namnet `forsta_sista.sh`:
   ```bash
   nano forsta_sista.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   if [ $# -eq 0 ]; then
       echo "Ange minst ett argument!"
       exit 1
   fi

   first=$1
   last=${!#}

   echo "Första argumentet: $first"
   echo "Sista argumentet: $last"
   ```
3. Spara, gör körbar och kör:
   ```bash
   chmod +x forsta_sista.sh
   ./forsta_sista.sh Alfa Beta Gamma Delta
   ```

**Förklaring:**  
- `$1` är det första argumentet, medan `${!#}` hämtar det sista.

**Utmaning:** Visa även det näst sista argumentet.

---

## Övning 8.18: Iterera genom argumenten med en while-loop

**Mål:**  
- Visa hur man med en while-loop itererar genom alla argument.

**Steg för steg:**
1. Skapa en fil med namnet `while_loop.sh`:
   ```bash
   nano while_loop.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   i=1
   while [ $i -le $# ]
   do
       eval "arg=\$$i"
       echo "Argument $i: $arg"
       i=$((i+1))
   done
   ```
3. Spara, gör körbar och kör:
   ```bash
   chmod +x while_loop.sh
   ./while_loop.sh Red Blue Green
   ```

**Förklaring:**  
- En while-loop används tillsammans med `eval` för att hämta varje argument baserat på dess index.

**Utmaning:** Modifiera loopen så att den skriver ut argumenten i omvänd ordning med en while-loop.

---

## Övning 8.19: Dela upp argument som innehåller flera ord

**Mål:**  
- Hantera argument där användaren kan skriva in text med mellanslag (som är inneslutna i citationstecken).

**Steg för steg:**
1. Skapa en fil med namnet `komplext_arg.sh`:
   ```bash
   nano komplext_arg.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Antal argument: $#"
   echo "Argumenten är:"
   for arg in "$@"
   do
       echo "$arg"
   done
   ```
3. Spara, gör körbar och testa:
   ```bash
   chmod +x komplext_arg.sh
   ./komplext_arg.sh "Hello World" "Good Morning" Evening
   ```

**Förklaring:**  
- När argument innehåller mellanslag bör de vara inom citationstecken så att de behandlas som en enhet.

**Utmaning:** Lägg till en utskrift som visar varje argument i citattecken.

---

## Övning 8.20: Avslutande interaktivt skript med argument och extra input

**Mål:**  
- Kombinera användarinput med argument: om inga argument anges, be användaren mata in några namn manuellt.

**Steg för steg:**
1. Skapa en fil med namnet `interaktiv_arg.sh`:
   ```bash
   nano interaktiv_arg.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   if [ $# -eq 0 ]; then
       echo "Inga namn angivna som argument."
       echo "Skriv in tre namn:"
       read namn1
       read namn2
       read namn3
       set -- $namn1 $namn2 $namn3
   fi

   echo "Du angav $# namn. Här är hälsningen:"
   i=1
   for namn in "$@"
   do
       echo "$i. Hej, $namn!"
       ((i++))
   done
   ```
3. Spara, gör körbar och testa:
   ```bash
   chmod +x interaktiv_arg.sh
   ./interaktiv_arg.sh
   # Eller testa med argument: ./interaktiv_arg.sh Lisa Karl Maria
   ```

**Förklaring:**  
- Om inga argument anges används `read` för att få in namn.  
- `set --` sätter de inmatade värdena som argument, så att resten av skriptet kan behandla dem som vanligt.

**Utmaning:** Lägg till ett extra meddelande som säger "Tack för att du delade med dig!" efter listan.
