# Hantera textfiler – skapa och skriva till en fil

Målet är att du ska bli trygg med att använda kommandon som `touch`, `echo`, `>>`, `>`, `cat`, `rm` och `read`. Dessa övningar är anpassade för 12-åringar som är helt nybörjare!

> **Allmänt tips:**  
> - Öppna terminalen (t.ex. med WSL Ubuntu) och gå till din hemkatalog med:
>   ```bash
>   cd ~
>   ```  
> - Använd en textredigerare (t.ex. `nano`) för att skapa och redigera dina skript.  
> - Spara filen, gör den körbar med:
>   ```bash
>   chmod +x skriptets_namn.sh
>   ```  
> - Kör sedan skriptet med:
>   ```bash
>   ./skriptets_namn.sh
>   ```

---

## Övning 5.1: Skapa en enkel textfil  
**Mål:** Skapa en fil med en rad text med hjälp av `echo` och omdirigering (`>`).

**Steg för steg:**
1. Öppna terminalen och gå till hemkatalogen:
   ```bash
   cd ~
   ```
2. Skapa ett nytt skript med namnet `ovning5_1.sh`:
   ```bash
   nano ovning5_1.sh
   ```
3. Skriv in följande kod:
   ```bash
   #!/bin/bash
   echo "Skapar filen min_textfil.txt..."
   echo "Detta är den första raden." > min_textfil.txt
   echo "Filen min_textfil.txt har skapats."
   ```
4. Spara (CTRL+O, Enter) och avsluta (CTRL+X).

5. Gör skriptet körbart och kör det:
   ```bash
   chmod +x ovning5_1.sh
   ./ovning5_1.sh
   ```

**Förklaring:**  
- `echo "text" > filnamn` skapar filen och skriver in texten (om filen redan finns skrivs den över).

**Utmaning:** Prova att ändra texten i filen till något annat.

---

## Övning 5.2: Lägg till ytterligare en rad  
**Mål:** Använd `>>` för att lägga till text i samma fil utan att radera tidigare innehåll.

**Steg för steg:**
1. Skapa ett skript med namnet `ovning5_2.sh`:
   ```bash
   nano ovning5_2.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Detta är den första raden." > min_textfil.txt
   echo "Detta är den andra raden." >> min_textfil.txt
   echo "Två rader har skrivits till min_textfil.txt."
   ```
3. Spara, gör körbart och kör skriptet:
   ```bash
   chmod +x ovning5_2.sh
   ./ovning5_2.sh
   ```

**Förklaring:**  
- `>>` lägger till text i slutet av filen utan att ta bort tidigare innehåll.

**Utmaning:** Lägg till en tredje rad med en annan text.

---

## Övning 5.3: Skapa en fil med flera rader  
**Mål:** Skapa en fil och skriv in tre rader text.

**Steg för steg:**
1. Skapa ett skript med namnet `ovning5_3.sh`:
   ```bash
   nano ovning5_3.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   rm -f min_textfil.txt  # Ta bort filen om den finns
   echo "Detta är den första raden." > min_textfil.txt
   echo "Detta är den andra raden." >> min_textfil.txt
   echo "Detta är den tredje raden." >> min_textfil.txt
   echo "Filen min_textfil.txt har nu tre rader."
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `rm -f` används för att radera en eventuell gammal version av filen innan vi skriver ny text.

**Utmaning:** Ändra texten i någon av raderna och kör skriptet igen.

---

## Övning 5.4: Visa innehållet i filen med `cat`  
**Mål:** Visa filens innehåll direkt i terminalen.

**Steg för steg:**
1. Skapa ett skript med namnet `ovning5_4.sh`:
   ```bash
   nano ovning5_4.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Innehållet i min_textfil.txt är:"
   cat min_textfil.txt
   ```
3. Spara, gör körbar och kör skriptet:
   ```bash
   chmod +x ovning5_4.sh
   ./ovning5_4.sh
   ```

**Förklaring:**  
- `cat filnamn` skriver ut hela filens innehåll i terminalen.

**Utmaning:** Kombinera med tidigare övning så att du först skapar filen och sedan visar dess innehåll.

---

## Övning 5.5: Skapa ett skript som skapar och visar en fil  
**Mål:** Kombinera skapande av fil med utskrift av innehållet.

**Steg för steg:**
1. Skapa ett skript med namnet `ovning5_5.sh`:
   ```bash
   nano ovning5_5.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   rm -f dagbok.txt
   echo "Idag har jag lärt mig att använda bash!" > dagbok.txt
   echo "Senare lade jag till mer text:" >> dagbok.txt
   echo "Bash är riktigt kul att lära sig!" >> dagbok.txt
   echo "Innehållet i dagboken är:"
   cat dagbok.txt
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Detta skript skapar en "dagbok" med flera rader och visar sedan innehållet.

**Utmaning:** Lägg till en extra rad där du skriver ut ett extra meddelande.

---

## Övning 5.6: Låt användaren välja filnamn  
**Mål:** Använd `read` för att låta användaren ange vilket filnamn som ska användas.

**Steg för steg:**
1. Skapa ett skript med namnet `interaktiv_textfil.sh`:
   ```bash
   nano interaktiv_textfil.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Ange ett filnamn (t.ex. min_dagboksfil.txt):"
   read filnamn
   rm -f $filnamn
   echo "Detta är den första raden i din fil." > $filnamn
   echo "Detta är den andra raden." >> $filnamn
   echo "Filen $filnamn har skapats med följande innehåll:"
   cat $filnamn
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `read filnamn` tar emot användarens inmatning och lagrar den i variabeln `filnamn`.

**Utmaning:** Be användaren att skriva in ytterligare en rad och sedan visa filens innehåll igen.

---

## Övning 5.7: Radera en fil innan ny skapelse  
**Mål:** Säkerställ att en gammal version av filen tas bort innan ny text skrivs in.

**Steg för steg:**
1. Skapa ett skript med namnet `radera_och_skriv.sh`:
   ```bash
   nano radera_och_skriv.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   filnamn="min_textfil.txt"
   echo "Tar bort filen $filnamn om den finns..."
   rm -f $filnamn
   echo "Skapar filen $filnamn med ny text..."
   echo "Detta är en ny start." > $filnamn
   echo "Filen har skapats med följande innehåll:"
   cat $filnamn
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `rm -f` tar bort filen tyst om den redan finns, vilket säkerställer att vi börjar fräscht.

**Utmaning:** Lägg till ytterligare en `echo`-rad för att informera om att filen raderades.

---

## Övning 5.8: Lägg till text i en befintlig fil  
**Mål:** Använd `>>` för att lägga till ytterligare text i en redan existerande fil.

**Steg för steg:**
1. Skapa ett skript med namnet `lagg_till_text.sh`:
   ```bash
   nano lagg_till_text.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   filnamn="dagbok.txt"
   echo "Detta är en ny rad som läggs till." >> $filnamn
   echo "Uppdaterat innehåll i $filnamn:"
   cat $filnamn
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `>>` lägger till text i slutet av filen utan att radera befintligt innehåll.

**Utmaning:** Prova att lägga till två nya rader med olika texter.

---

## Övning 5.9: Skapa en fil med dynamiskt innehåll i en loop  
**Mål:** Använd en for‑loop för att skapa en fil där varje rad innehåller loopens nummer.

**Steg för steg:**
1. Skapa ett skript med namnet `loop_innehall.sh`:
   ```bash
   nano loop_innehall.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   rm -f nummer.txt
   echo "Skapar filen nummer.txt med 10 rader..."
   for i in {1..10}
   do
       echo "Detta är rad nummer $i" >> nummer.txt
   done
   echo "Innehållet i nummer.txt:"
   cat nummer.txt
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Varje iteration i loopen lägger till en rad med det aktuella numret.

**Utmaning:** Ändra loopen så att den skapar 15 rader istället.

---

## Övning 5.10: Skapa en fil med en rubrik och flera rader text  
**Mål:** Skriv en rubrik i filen följt av flera text-rader med `>>`.

**Steg för steg:**
1. Skapa ett skript med namnet `rubrik_och_text.sh`:
   ```bash
   nano rubrik_och_text.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   filnamn="rapport.txt"
   rm -f $filnamn
   echo "=== Min Rapport ===" > $filnamn
   echo "Första stycket i rapporten." >> $filnamn
   echo "Andra stycket i rapporten." >> $filnamn
   echo "Rapporten har skapats. Innehållet är:"
   cat $filnamn
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Rubriken skrivs med `>`, medan de följande raderna läggs till med `>>`.

**Utmaning:** Lägg till en tredje stycke-rad.

---

## Övning 5.11: Skapa en interaktiv "dagbok"  
**Mål:** Låt användaren mata in en rad text som sparas i en fil.

**Steg för steg:**
1. Skapa ett skript med namnet `dagbok.sh`:
   ```bash
   nano dagbok.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Skriv en dagboksrad:"
   read rad
   echo $rad >> dagbok.txt
   echo "Din rad har lagts till i dagboken. Nu innehåller filen:"
   cat dagbok.txt
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Användaren matar in en rad som sedan läggs till i filen `dagbok.txt` med `>>`.

**Utmaning:** Låt användaren skriva in tre rader (anropa `read` tre gånger) och skriv sedan ut hela dagboken.

---

## Övning 5.12: Kombinera två filer med `cat`  
**Mål:** Skapa två filer och sedan slå samman dem till en tredje fil.

**Steg för steg:**
1. Skapa ett skript med namnet `sammanfoga.sh`:
   ```bash
   nano sammanfoga.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Innehåll i fil A:" > filA.txt
   echo "Detta är text från fil A." >> filA.txt

   echo "Innehåll i fil B:" > filB.txt
   echo "Detta är text från fil B." >> filB.txt

   cat filA.txt filB.txt > filC.txt

   echo "Filerna A och B har sammanfogats till filC.txt. Innehållet i filC.txt är:"
   cat filC.txt
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `cat filA.txt filB.txt > filC.txt` slår samman innehållet i fil A och fil B till fil C.

**Utmaning:** Lägg till en extra rad mellan innehållet från de två filerna i fil C.

---

## Övning 5.13: Skapa en fil med användarens hälsning  
**Mål:** Använd `read` för att få en hälsning från användaren och skriv den i en fil.

**Steg för steg:**
1. Skapa ett skript med namnet `helsning.sh`:
   ```bash
   nano helsning.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Skriv din hälsning:"
   read hälsning
   rm -f hälsning.txt
   echo $hälsning > hälsning.txt
   echo "Din hälsning sparades i hälsning.txt:"
   cat hälsning.txt
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Skriptet tar emot en hälsning från användaren och sparar den i en fil.

**Utmaning:** Låt användaren skriva in två hälsningsrader och visa båda.

---

## Övning 5.14: Skriv ut filinnehåll med en rubrik  
**Mål:** Skapa en fil med en rubrik och skriv sedan ut innehållet med rubriken.

**Steg för steg:**
1. Skapa ett skript med namnet `rubrik_fil.sh`:
   ```bash
   nano rubrik_fil.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   filnamn="minrapport.txt"
   rm -f $filnamn
   echo "=== Rapport ===" > $filnamn
   echo "Detta är rapportens innehåll." >> $filnamn
   echo "Rapporten visas nedan:"
   cat $filnamn
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Rubriken separeras från innehållet med `>` respektive `>>`.

**Utmaning:** Lägg till en extra underrubrik efter rubriken.

---

## Övning 5.15: Skapa en fil med två olika typer av text  
**Mål:** Skapa en fil där första halvan innehåller en text och andra halvan en annan text.

**Steg för steg:**
1. Skapa ett skript, t.ex. `tvådelad_fil.sh`:
   ```bash
   nano tvådelad_fil.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   filnamn="tvådelad.txt"
   rm -f $filnamn
   echo "Detta är den första delen av filen." > $filnamn
   echo "-----" >> $filnamn
   echo "Detta är den andra delen av filen." >> $filnamn
   echo "Filen $filnamn har skapats med två delar:"
   cat $filnamn
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- En rad med "-----" används för att visuellt dela upp innehållet.

**Utmaning:** Ändra avgränsaren till något annat, t.ex. "********".

---

## Övning 5.16: Lägg till text med en extra tom rad emellan  
**Mål:** Skapa en fil där varje ny textsektion åtföljs av en tom rad.

**Steg för steg:**
1. Skapa ett skript, t.ex. `tomrad_tillagd.sh`:
   ```bash
   nano tomrad_tillagd.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   filnamn="med_tomrad.txt"
   rm -f $filnamn
   echo "Första texten." > $filnamn
   echo "" >> $filnamn
   echo "Andra texten efter en tom rad." >> $filnamn
   echo "Innehållet i $filnamn:"
   cat $filnamn
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `echo ""` skapar en tom rad i filen.

**Utmaning:** Prova att lägga till två tomma rader istället.

---

## Övning 5.17: Skapa en fil med användarbestämt innehåll  
**Mål:** Låt användaren skriva in en rad som sedan sparas i en fil.

**Steg för steg:**
1. Skapa ett skript, t.ex. `skriv_innehall.sh`:
   ```bash
   nano skriv_innehall.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Skriv in en rad text som ska sparas i filen:"
   read text
   filnamn="anvandarinnehall.txt"
   rm -f $filnamn
   echo $text > $filnamn
   echo "Din text har sparats i $filnamn. Innehållet är:"
   cat $filnamn
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Detta skript tar in en rad från användaren och sparar den.

**Utmaning:** Låt användaren skriva in två rader (anropa `read` två gånger) och skriv ut båda.

---

## Övning 5.18: Kopiera innehållet i en fil till en ny fil  
**Mål:** Skapa en fil och kopiera dess innehåll till en annan fil med `cat`.

**Steg för steg:**
1. Skapa ett skript, t.ex. `kopiera_innehall.sh`:
   ```bash
   nano kopiera_innehall.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Detta är originaltexten." > original.txt
   echo "Kopierar innehållet från original.txt till kopia.txt..."
   cat original.txt > kopia.txt
   echo "Innehållet i kopia.txt är:"
   cat kopia.txt
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- `cat original.txt > kopia.txt` kopierar innehållet från en fil till en annan.

**Utmaning:** Använd `>>` istället för `>` för att se skillnaden om kopia.txt redan innehåller text.

---

## Övning 5.19: Lägg till datum (en fast text) i filnamnet vid skapelse  
**Mål:** Skapa en fil där filnamnet får en fast text (t.ex. "_2025") tillagt.

**Steg för steg:**
1. Skapa ett skript, t.ex. `datum_tillagd.sh`:
   ```bash
   nano datum_tillagd.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   filnamn="rapport.txt"
   nyttFilnamn="rapport_2025.txt"
   rm -f $filnamn $nyttFilnamn
   echo "Detta är en rapport." > $filnamn
   cp $filnamn $nyttFilnamn
   echo "Filen kopierades med datum till $nyttFilnamn. Innehållet är:"
   cat $nyttFilnamn
   ```
3. Spara, gör körbar och kör skriptet.

**Förklaring:**  
- Här används `cp` för att skapa en kopia med ett nytt filnamn.

**Utmaning:** Byt ut "_2025" mot ett annat fast värde, t.ex. "_v1".

---

## Övning 5.20: Avslutande interaktivt skript – Välj mellan att skriva över eller lägga till  
**Mål:** Skapa ett komplett skript där användaren får välja om filen ska skrivas över eller om text ska läggas till.

**Steg för steg:**
1. Skapa ett skript, t.ex. `interaktiv_text.sh`:
   ```bash
   nano interaktiv_text.sh
   ```
2. Skriv in:
   ```bash
   #!/bin/bash
   echo "Ange filnamn:"
   read filnamn

   echo "Vill du skriva över filen eller lägga till text? (skriv 'överskriv' eller 'lägg till')"
   read val

   if [ "$val" = "överskriv" ]; then
       echo "Skriv den nya texten:"
       read text
       echo "$text" > $filnamn
       echo "Filen $filnamn skrivs över med den nya texten."
   else
       echo "Skriv texten du vill lägga till:"
       read text
       echo "$text" >> $filnamn
       echo "Texten har lagts till i filen $filnamn."
   fi

   echo "Det aktuella innehållet i $filnamn är:"
   cat $filnamn
   ```
3. Spara, gör körbar med:
   ```bash
   chmod +x interaktiv_text.sh
   ```
   och kör skriptet:
   ```bash
   ./interaktiv_text.sh
   ```

**Förklaring:**  
- Skriptet låter användaren välja mellan att skriva över en fil eller lägga till text i den, och visar sedan filens innehåll.

**Utmaning:** Lägg till en extra fråga där användaren får skriva in en rubrik som läggs till i början av filen (använd `>`) och sedan läggs resten av texten med `>>`.
