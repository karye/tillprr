# Introduktion till terminalen, Bash och skriptning i Linux

Linux-terminalen är ett kraftfullt verktyg som används för att styra datorn genom textbaserade kommandon. Till skillnad från ett grafiskt användargränssnitt (GUI) där man klickar runt, kan man här skriva in kommandon direkt för att utföra uppgifter.  

I denna introduktion går vi igenom:

- Vad terminalen är och hur man använder den.
- Vad Bash är och varför det är viktigt.
- Hur man skapar och kör egna Bash-skript.

## 1. Terminalen – din kommandocentral
En terminal är ett program som låter dig skriva in textbaserade kommandon för att styra datorn. Om du använder **WSL (Windows Subsystem for Linux)**, kan du starta terminalen genom att öppna "Ubuntu" i Windows.

**Vanliga terminalkommandon:**  
Här är några grundläggande kommandon som du kan testa:

| Kommando        | Funktion                                     |
|----------------|---------------------------------------------|
| `pwd`         | Visar var du befinner dig i filsystemet. |
| `ls`          | Listar filer och mappar i aktuell katalog. |
| `cd mappnamn` | Går in i en specifik mapp.               |
| `mkdir test`  | Skapar en ny mapp som heter "test".      |
| `touch fil.txt` | Skapar en tom fil som heter "fil.txt".  |
| `rm fil.txt`  | Raderar en fil.                          |
| `rmdir test`  | Raderar en tom mapp.                     |

## 2. Vad är Bash?
Bash (Bourne Again Shell) är ett kommandotolkprogram i Linux. Det används för att tolka och köra kommandon i terminalen.  
Förutom att köra enstaka kommandon kan vi skapa **skript**, vilket är en samling av kommandon som körs i följd.

**Exempel på ett enkelt kommando i Bash:**
```bash
echo "Hej, världen!"
```
Detta skriver ut texten `"Hej, världen!"` i terminalen.

## 3. Vad är ett Bash-skript?
Ett Bash-skript är en textfil där vi sparar kommandon som kan köras automatiskt. Bash-skript används ofta för att:
- Automatisera uppgifter.
- Skapa anpassade kommandon.
- Hantera filer och mappar snabbare.

### Skapa och köra ett Bash-skript

1. **Skapa ett nytt skript**  
   Öppna terminalen och skriv:
   ```bash
   nano hej.sh
   ```
   Detta öppnar texteditorn Nano där vi kan skriva vår kod.

2. **Skriv in följande kod:**
   ```bash
   #!/bin/bash
   echo "Hej, världen!"
   ```
   **Förklaring:**
   - `#!/bin/bash` talar om att detta är ett Bash-skript.
   - `echo "Hej, världen!"` skriver ut text i terminalen.

3. **Spara och avsluta:**  
   - Tryck `CTRL + O` för att spara.
   - Tryck `Enter` för att bekräfta.
   - Tryck `CTRL + X` för att stänga Nano.

4. **Gör skriptet körbart:**  
   ```bash
   chmod +x hej.sh
   ```
   Detta gör att vi kan köra skriptet.

5. **Kör skriptet:**  
   ```bash
   ./hej.sh
   ```
   **Utmatning:**
   ```
   Hej, världen!
   ```

## 4. Variabler i Bash
Variabler används för att lagra information som kan användas senare.

**Exempel:**
```bash
#!/bin/bash
namn="Alex"
echo "Hej, $namn!"
```
**Utmatning:**
```
Hej, Alex!
```

## 5. Läsa in data från användaren
Vi kan låta användaren skriva in information med `read`.

**Exempel:**
```bash
#!/bin/bash
echo "Vad heter du?"
read namn
echo "Hej, $namn!"
```
**Utmatning vid körning:**
```
Vad heter du?
> Lisa
Hej, Lisa!
```

## 6. If-satser – fatta beslut
Med en `if`-sats kan vi låta skriptet reagera på indata.

**Exempel:**
```bash
#!/bin/bash
echo "Hur gammal är du?"
read ålder

if [ "$ålder" -ge 18 ]; then
    echo "Du är myndig!"
else
    echo "Du är fortfarande under 18 år."
fi
```

## 7. Loopar – repetera kommandon
Loopar används för att köra samma kod flera gånger.

**Exempel på en `for`-loop:**
```bash
#!/bin/bash
for i in {1..5}
do
    echo "Detta är rad nummer $i"
done
```
**Utmatning:**
```
Detta är rad nummer 1
Detta är rad nummer 2
Detta är rad nummer 3
Detta är rad nummer 4
Detta är rad nummer 5
```

## 8. Skript med argument
Vi kan skicka in data när vi kör skriptet.

**Exempel:**
```bash
#!/bin/bash
echo "Hej, $1!"
```
Kör skriptet med:
```bash
./script.sh Alex
```
**Utmatning:**
```
Hej, Alex!
```

## Sammanfattning
- **Terminalen** används för att köra kommandon.
- **Bash** är ett programmeringsspråk för att automatisera uppgifter.
- **Skript** är en serie av kommandon som sparas i en fil.
- **Variabler** lagrar information.
- **If-satser** gör det möjligt att ta beslut i skript.
- **Loopar** gör att vi kan upprepa kommandon.
- **Argument** gör att vi kan skicka in data till skriptet vid körning.

### Nästa steg
Om du vill fördjupa dig kan du testa att:
✅ Göra skript interaktiva med `read`.  
✅ Skapa skript som organiserar filer och mappar.  
✅ Använda `if`-satser och loopar för mer avancerad funktionalitet.  
