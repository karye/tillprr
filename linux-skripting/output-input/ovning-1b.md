# Övningsuppgifter i Bash

Här går vi igenom grunderna i Bash-programmering genom att skapa enkla skript. Varje steg innehåller en kort beskrivning, en uppgift och en utmaning. Vi kan använda Windows Subsystem for Linux (WSL) eller en annan terminal för att köra skripten.

## Övningar med användarinput

> **Viktigt:** Kommandot `read` används för att vänta på att användaren ska skriva in något. Det som skrivs in sparas i en variabel (t.ex. `namn`), som du sedan kan använda med prefixet `$` (t.ex. `$namn`).

---

### Steg 21: Hälsa med användarens namn
**Mål:** Läs in användarens namn och skriv ut en hälsning.

1. Skapa filen `input21.sh`:
   ```bash
   nano input21.sh
   ```
2. Skriv in följande kod:
   ```bash
   #!/bin/bash
   echo "Vad heter du?"
   read namn
   echo "Hej, $namn!"
   ```
3. Spara, avsluta, gör skriptet körbart och kör det:
   ```bash
   bash input21.sh
   ```
   
**Förklaring:**  
- `read namn` väntar på att du skriver in ditt namn.  
- Variabeln `namn` används sedan i utskriften med `$namn`.

**Utmaning:** Prova att ändra hälsningen, t.ex. "Välkommen, $namn!"

---

### Steg 22: För- och efternamn
**Mål:** Läs in både förnamn och efternamn och skriv ut en komplett hälsning.

1. Skapa filen `input22.sh`:
   ```bash
   nano input22.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Vad är ditt förnamn?"
   read fornamn
   echo "Vad är ditt efternamn?"
   read efternamn
   echo "Hej, $fornamn $efternamn!"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input22.sh
   ```

**Förklaring:**  
Vi använder två variabler, `fornamn` och `efternamn`, för att samla in och sedan kombinera informationen.

**Utmaning:** Lägg till en rad som skriver ut ditt fulla namn baklänges (du kan experimentera med texten, även om det blir manuellt).

---

### Steg 23: Favoritfilm
**Mål:** Fråga användaren om deras favoritfilm och skriv ut ett meddelande.

1. Skapa filen `input23.sh`:
   ```bash
   nano input23.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Vad är din favoritfilm?"
   read film
   echo "Wow, $film verkar vara en grym film!"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input23.sh
   ```

**Förklaring:**  
Här läser vi in filmens namn i variabeln `film` och använder den i vårt meddelande.

**Utmaning:** Byt ut meddelandet mot något eget, t.ex. "Jag vill också se $film!"

---

### Steg 24: Favoritfärg
**Mål:** Läs in användarens favoritfärg och skriv ut ett positivt meddelande.

1. Skapa filen `input24.sh`:
   ```bash
   nano input24.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Vilken är din favoritfärg?"
   read farg
   echo "Så fint med färgen $farg!"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input24.sh
   ```

**Förklaring:**  
Variabeln `farg` sparar användarens svar och används i utskriften.

**Utmaning:** Lägg till en fråga till, t.ex. "Varför gillar du $farg?" (skriv bara ut frågan).

---

### Steg 25: Favoritmat
**Mål:** Fråga användaren om deras favoritmat och skriv ut ett meddelande.

1. Skapa filen `input25.sh`:
   ```bash
   nano input25.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Vilken är din favoritmat?"
   read mat
   echo "Mums, $mat låter jättegott!"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input25.sh
   ```

**Förklaring:**  
Med `read mat` sparas användarens svar i variabeln `mat`, som sedan visas med ett uppmuntrande meddelande.

**Utmaning:** Försök att lägga till en extra rad som säger "Jag vill gärna prova $mat någon gång!"

---

### Steg 26: Upprepa ett meddelande
**Mål:** Låt användaren skriva in ett meddelande och skriv ut det.

1. Skapa filen `input26.sh`:
   ```bash
   nano input26.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Skriv något roligt:"
   read text
   echo "Du skrev: $text"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input26.sh
   ```

**Förklaring:**  
Här övar du på att läsa in och visa precis det som användaren skriver in.

**Utmaning:** Ändra så att skriptet skriver ut "Vad kul att du skrev: $text" istället.

---

### Steg 27: Kombinera namn och ålder
**Mål:** Fråga efter både namn och ålder och kombinera dem i en hälsning.

1. Skapa filen `input27.sh`:
   ```bash
   nano input27.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Vad heter du?"
   read namn
   echo "Hur gammal är du?"
   read alder
   echo "Hej, $namn! Du är $alder år gammal."
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input27.sh
   ```

**Förklaring:**  
Genom att använda två `read`-kommandon kan vi ta in flera uppgifter och visa dem tillsammans.

**Utmaning:** Prova att lägga till en rad som säger "Vad roligt att du är $alder år!"

---

### Steg 28: Fråga om tre favoritsaker
**Mål:** Läs in tre olika favoritsaker och skriv ut en sammanfattande mening.

1. Skapa filen `input28.sh`:
   ```bash
   nano input28.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Skriv din favoritsport:"
   read sport
   echo "Skriv ditt favoritdjur:"
   read djur
   echo "Skriv din favoritmusikgenre:"
   read musik
   echo "Din favoritsport är $sport, du gillar $djur och din favoritmusik är $musik!"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input28.sh
   ```

**Förklaring:**  
Detta skript visar hur du kan läsa in flera värden och sedan använda dem tillsammans i ett meddelande.

**Utmaning:** Lägg till en fråga till om favoritmat och kombinera alla fyra svar i utskriften.

---

### Steg 29: Skapa en dikt med dina ord
**Mål:** Låt användaren mata in ord som används för att skapa en liten dikt.

1. Skapa filen `input29.sh`:
   ```bash
   nano input29.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Skriv in ett adjektiv (t.ex. vacker):"
   read adjektiv
   echo "Skriv in ett substantiv (t.ex. blomma):"
   read substantiv
   echo "Skriv in ett verb (t.ex. dofta):"
   read verb
   echo "Din dikt: Den $adjektiv $substantiv börjar att $verb i vinden."
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input29.sh
   ```

**Förklaring:**  
Genom att använda variabler kan du skapa dynamiska meddelanden – i detta fall en dikt med ord som användaren väljer.

**Utmaning:** Ändra diktens uppbyggnad och lägg till en extra rad.

---

### Steg 30: Hemligt meddelande
**Mål:** Fråga användaren efter ett hemligt meddelande och skriv ut det.

1. Skapa filen `input30.sh`:
   ```bash
   nano input30.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Skriv ett hemligt meddelande:"
   read hemlighet
   echo "Ditt hemliga meddelande är: $hemlighet"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input30.sh
   ```

**Förklaring:**  
Detta skript visar hur man kan ta emot och sedan återge användarens input – ett enkelt sätt att skapa interaktivitet.

**Utmaning:** Lägg till en rad som säger "Ditt meddelande är hemligt, men ändå superviktigt!"

---

### Steg 31: Välj ett drömmande djur
**Mål:** Fråga vilket djur användaren skulle vilja vara och skriv ut ett roligt meddelande.

1. Skapa filen `input31.sh`:
   ```bash
   nano input31.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Vilket djur skulle du vilja vara för en dag?"
   read djur
   echo "Wow, att vara en $djur skulle vara spännande!"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input31.sh
   ```

**Förklaring:**  
Variabeln `djur` används här för att skapa en fantasifull och rolig utskrift.

**Utmaning:** Byt ut meddelandet mot "Att vara en $djur låter som en riktig äventyrsdag!"

---

### Steg 32: Favoritbok
**Mål:** Läs in användarens favoritbok och skriv ut ett bekräftande meddelande.

1. Skapa filen `input32.sh`:
   ```bash
   nano input32.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Vad är din favoritbok?"
   read bok
   echo "Jag gillar också boken $bok!"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input32.sh
   ```

**Förklaring:**  
En enkel användning av `read` där svaret lagras i variabeln `bok` och sedan skrivs ut.

**Utmaning:** Försök att fråga även om en favoritförfattare och inkludera det i meddelandet.

---

### Steg 33: Kombinera intressen
**Mål:** Fråga användaren om två olika intressen och kombinera dem i ett meddelande.

1. Skapa filen `input33.sh`:
   ```bash
   nano input33.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Skriv in din favoritsport:"
   read sport
   echo "Skriv in din favoritmat:"
   read favoritmat
   echo "Att spela $sport och äta $favoritmat låter som en rolig dag!"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input33.sh
   ```

**Förklaring:**  
Detta skript visar hur du kan kombinera två användarinput för att skapa ett komplett meddelande.

**Utmaning:** Lägg till en fråga om favoritmusik och inkludera det i utskriften.

---

### Steg 34: Enkel dialog
**Mål:** Starta en mini-konversation med användaren genom att fråga hur de mår.

1. Skapa filen `input34.sh`:
   ```bash
   nano input34.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Hur mår du idag?"
   read mal
   echo "Du sa att du mår $mal. Hoppas att du får en bra dag!"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input34.sh
   ```

**Förklaring:**  
Med `read mal` får vinga in hur användaren mår och kan svara med ett uppmuntrande meddelande.

**Utmaning:** Försök att ändra frågan till "Hur känner du dig idag?" och anpassa svaret.

---

### Steg 35: Framtidsdrömmar
**Mål:** Fråga användaren om deras framtidsdrömmar och skriv ut ett inspirerande meddelande.

1. Skapa filen `input35.sh`:
   ```bash
   nano input35.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Vad drömmer du om att bli när du blir stor?"
   read dream
   echo "Att bli $dream låter som en fantastisk framtid!"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input35.sh
   ```

**Förklaring:**  
Variabeln `dream` sparar användarens dröm, och vi använder den för att skapa ett positivt och inspirerande meddelande.

**Utmaning:** Lägg till en extra rad som säger "Följ din dröm och bli den bästa $dream du kan vara!"

---

### Steg 36: Enkel matte-input
**Mål:** Be användaren skriva in ett tal och skriv ut det.

1. Skapa filen `input36.sh`:
   ```bash
   nano input36.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Skriv ett tal:"
   read tal
   echo "Talet du skrev är: $tal"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input36.sh
   ```

**Förklaring:**  
Här lär du dig att läsa in numerisk input, även om vi inte gör beräkningar än.

**Utmaning:** Fråga sedan "Är inte $tal ett fantastiskt tal?" och skriv ut det.

---

### Steg 37: Kombinera två meddelanden
**Mål:** Fråga efter två olika uppgifter och kombinera dem i ett meddelande.

1. Skapa filen `input37.sh`:
   ```bash
   nano input37.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Vad är ditt namn?"
   read namn
   echo "Vad är din favoritmat?"
   read favoritmat
   echo "$namn, din favoritmat är verkligen god, eller hur?"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input37.sh
   ```

**Förklaring:**  
Detta skript visar hur du kan kombinera två olika variabler för att skapa ett personligt meddelande.

**Utmaning:** Lägg till en fråga om favoritfilm och inkludera även det i utskriften.

---

### Steg 38: Skapa en mini-konversation
**Mål:** Låt skriptet inleda en liten konversation med användaren.

1. Skapa filen `input38.sh`:
   ```bash
   nano input38.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Hej! Vad heter du?"
   read namn
   echo "Trevligt att träffas, $namn!"
   echo "Hur känner du dig idag?"
   read sinnesstamning
   echo "Jag hoppas att du känner dig $sinnesstamning hela dagen!"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input38.sh
   ```

**Förklaring:**  
Här får vi en liten dialog där vi läser in både namn och stämning och ger ett personligt svar.

**Utmaning:** Försök att ändra dialogen så att du även frågar "Vad har du för planer idag?" och skriv ut svaret.

---

### Steg 39: Enkel ramsa med input
**Mål:** Använd input från användaren för att skapa en liten ramsa.

1. Skapa filen `input39.sh`:
   ```bash
   nano input39.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Skriv in ett ord som rimmar på 'sol':"
   read rimord
   echo "Solen ler, och $rimord är kul att höra!"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input39.sh
   ```

**Förklaring:**  
Detta skript visar att man även kan använda inmatad text i roliga och kreativa sammanhang, som en ramsa.

**Utmaning:** Prova att fråga efter ett ord som rimmar på "blom" och skapa en ny ramsa med det.

---

### Steg 40: Avslutningsskript med personlig hälsning
**Mål:** Kombinera flera inmatningar för att skapa en personlig hälsning med ett positivt budskap.

1. Skapa filen `input40.sh`:
   ```bash
   nano input40.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Vad heter du?"
   read namn
   echo "Skriv ett positivt meddelande:"
   read positiv
   echo "Hej, $namn! Kom ihåg: $positiv"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input40.sh
   ```

**Förklaring:**  
Detta avslutande skript visar hur du kan kombinera flera inmatningar för att skapa ett personligt och uppmuntrande meddelande. Du har nu lärt dig hur man tar emot input från användaren och använder variabler i utskrifter.

**Utmaning:** Anpassa meddelandet ytterligare, till exempel genom att lägga till en extra rad som säger "Ha en underbar dag, $namn!"

### Steg 41: Snyggare input

**Mål:** Läs in två ord och skriv ut dem på samma rad.

1. Skapa filen `input41.sh`:
   ```bash
   nano input41.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash

   echo "-n Ange ett ditt förnamn: "
   read fornamn
   echo "-n Ange ditt efternamn: "
   read efternamn
   echo "Ditt namn är: $fornamn $efternamn"
   ```

3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash input41.sh
   ```

### Steg 42: Input med text

**Mål:** Läs in en text och skriv ut den.

1. Skapa filen `input42.sh`:
   ```bash
   nano input42.sh
   ```

2. Skriv in koden:
   ```bash
   #!/bin/bash

   read -p "Skriv en text: " text
   echo "Du skrev: $text"
   ```