# Övningsuppgifter i Bash

Här går vi igenom grunderna i Bash-programmering genom att skapa enkla skript. Varje steg innehåller en kort beskrivning, en uppgift och en utmaning. Vi kan använda Windows Subsystem for Linux (WSL) eller en annan terminal för att köra skripten.

## Skriva enkla Bash-skript

> **Tips:**  
> - Använd terminalen (t.ex. WSL Ubuntu) och en textredigerare som _nano_.  
> - Kom ihåg att spara ändringarna och göra dina skript körbara med kommandot `bash filnamn.sh`.

---

### Steg 1: Skapa ditt första skript
**Mål:** Skapa ett skript som skriver ut "Hej, världen!"

1. Öppna terminalen och gå till hemkatalogen:
   ```bash
   cd ~
   ```
2. Skapa en fil som heter `hej.sh`:
   ```bash
   nano hej.sh
   ```
3. Skriv in följande kod:
   ```bash
   #!/bin/bash
   echo "Hej, världen!"
   ```
4. Kör skriptet:
   ```bash
   bash hej.sh
   ```

---

### Steg 2: Anpassa hälsningen
**Utmaning:** Ändra texten så att den skriver ut ditt namn, t.ex. "Hej, jag heter [Ditt Namn]!"  
_Försök själv genom att redigera `hej.sh` och ändra raden med `echo`._

---

### Steg 3: Skript med två rader
**Mål:** Skapa ett skript som skriver ut två rader med olika hälsningar.

1. Skapa filen `dubbel.sh`:
   ```bash
   nano dubbel.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Hej, världen!"
   echo "Välkommen till Bash!"
   ```
3. Spara, avsluta, gör skriptet körbart och kör det:
   ```bash
   bash dubbel.sh
   ```

---

### Steg 4: Infoga en tom rad
**Mål:** Skapa ett skript som skriver ut ett meddelande, en tom rad och sedan ett annat meddelande.

1. Skapa filen `tomrad.sh`:
   ```bash
   nano tomrad.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Första raden"
   echo ""
   echo "Tredje raden"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash tomrad.sh
   ```

---

### Steg 5: Tre meddelanden
**Mål:** Skapa ett skript som skriver ut tre olika meddelanden på tre rader.

1. Skapa filen `tre.sh`:
   ```bash
   nano tre.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Meddelande 1"
   echo "Meddelande 2"
   echo "Meddelande 3"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash tre.sh
   ```

---

### Steg 6: Lista med favoritdjur
**Mål:** Skapa ett skript som skriver ut en lista med tre favoritdjur, ett per rad.

1. Skapa filen `djur.sh`:
   ```bash
   nano djur.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "1. Hund"
   echo "2. Katt"
   echo "3. Fågel"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash djur.sh
   ```

---

### Steg 7: Skriv en enkel dikt
**Mål:** Skapa ett skript som skriver ut en dikt med fyra rader.

1. Skapa filen `dikt.sh`:
   ```bash
   nano dikt.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Rosen är röd,"
   echo "Violen är blå."
   echo "Solen skiner klart,"
   echo "och du är så grå (nä, bara skoj!)"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash dikt.sh
   ```

---

### Steg 8: Skapa en banner
**Mål:** Skapa ett skript som visar en banner med rubriken "BASH ÄR KUL!" omgiven av ramar.

1. Skapa filen `banner.sh`:
   ```bash
   nano banner.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "------------------"
   echo "  BASH ÄR KUL!  "
   echo "------------------"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash banner.sh
   ```

---

### Steg 9: Visa två citat
**Mål:** Skapa ett skript som skriver ut två olika citat.  
_Använd escape-tecken (\) om du vill inkludera citattecken i texten._

1. Skapa filen `citat.sh`:
   ```bash
   nano citat.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "\"Livet är en resa.\""
   echo "\"Våga drömma stort!\""
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash citat.sh
   ```

---

### Steg 10: Hälsning och påminnelse
**Mål:** Skapa ett skript som först hälsar på användaren och sedan ger en vänlig påminnelse.

1. Skapa filen `hälsning.sh`:
   ```bash
   nano hälsning.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Hej, vän!"
   echo "Kom ihåg att le idag!"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash hälsning.sh
   ```

---

### Steg 11: Morgon och kväll
**Mål:** Skapa ett skript som skriver ut "God morgon!" på första raden och "God natt!" på andra raden.

1. Skapa filen `dagens_hälsningar.sh`:
   ```bash
   nano dagens_hälsningar.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "God morgon!"
   echo "God natt!"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash dagens_hälsningar.sh
   ```

---

### Steg 12: En rolig ramsa
**Mål:** Skapa ett skript som skriver ut en kort ramsa.

1. Skapa filen `ramsa.sh`:
   ```bash
   nano ramsa.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Tick tack, tick tack, vem är på snack?"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash ramsa.sh
   ```

---

### Steg 13: Ditt namn på två rader
**Mål:** Skapa ett skript som skriver ut ditt förnamn på en rad och ditt efternamn på nästa.

1. Skapa filen `namn.sh`:
   ```bash
   nano namn.sh
   ```
2. Skriv in koden (ändra till ditt riktiga namn):
   ```bash
   #!/bin/bash
   echo "Förnamn: [Ditt förnamn]"
   echo "Efternamn: [Ditt efternamn]"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash namn.sh
   ```

---

### Steg 14: En fullständig mening
**Mål:** Skapa ett skript som skriver ut en lång mening.

1. Skapa filen `mening.sh`:
   ```bash
   nano mening.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Att lära sig bash är både roligt och spännande!"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash mening.sh
   ```

---

### Steg 15: Två meddelanden med mellanrum
**Mål:** Skapa ett skript som skriver ut två meddelanden med en tom rad mellan dem.

1. Skapa filen `två_meddelanden.sh`:
   ```bash
   nano två_meddelanden.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Första meddelandet"
   echo ""
   echo "Andra meddelandet"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash två_meddelanden.sh
   ```

---

### Steg 16: Lista med favoritmaträtter
**Mål:** Skapa ett skript som skriver ut en lista med dina tre favoritmaträtter.

1. Skapa filen `maträtter.sh`:
   ```bash
   nano maträtter.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "1. Pizza"
   echo "2. Hamburgare"
   echo "3. Sushi"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash maträtter.sh
   ```

---

### Steg 17: Repetera en uppmuntran
**Mål:** Skapa ett skript som skriver ut meddelandet "Bra jobbat!" tre gånger (varje gång med ett eget `echo`).

1. Skapa filen `repetera.sh`:
   ```bash
   nano repetera.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Bra jobbat!"
   echo "Bra jobbat!"
   echo "Bra jobbat!"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash repetera.sh
   ```

---

### Steg 18: Hälsning med en rolig kommentar
**Mål:** Skapa ett skript som först hälsar på användaren och sedan skriver ut en rolig kommentar.

1. Skapa filen `rolig.sh`:
   ```bash
   nano rolig.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Hej, kompis!"
   echo "Dags att ha kul och lära sig något nytt!"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash rolig.sh
   ```

---

### Steg 19: En enkel meny
**Mål:** Skapa ett skript som skriver ut en "meny" med tre alternativ.

1. Skapa filen `meny.sh`:
   ```bash
   nano meny.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "1. Starta"
   echo "2. Stoppa"
   echo "3. Hjälp"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash meny.sh
   ```

---

### Steg 20: Kombinera allt du lärt dig
**Mål:** Skapa ett skript som innehåller en välkomsttext, en lista med tre saker och en uppmuntrande avslutning.

1. Skapa filen `kombinerat.sh`:
   ```bash
   nano kombinerat.sh
   ```
2. Skriv in koden:
   ```bash
   #!/bin/bash
   echo "Välkommen till Bash-världen!"
   echo ""
   echo "Här är tre saker du kan göra:"
   echo "   1. Läsa en bok"
   echo "   2. Spela ett spel"
   echo "   3. Räkna stjärnor"
   echo ""
   echo "Ha en fantastisk dag!"
   ```
3. Spara, avsluta, gör körbart och kör:
   ```bash
   bash kombinerat.sh
   ```