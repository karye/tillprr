# Uppgifter: Linux-skripting

## Uppgift 1: skapa ett hälsningsskript
**Svårighetsgrad:** ⭐ (enkel)  

**Instruktion:**  
Skapa ett skript som tar emot ett namn som argument och skriver ut en hälsning. Om inget namn anges, ska skriptet skriva ut "Du måste ange ett namn!".  

**Exempel på körning:**  
```bash
./hälsa.sh Alex
```
**Utmatning:**  
```
Hej, Alex! Välkommen!
```
Om man kör skriptet utan argument:  
```bash
./hälsa.sh
```
**Utmatning:**  
```
Du måste ange ett namn!
```

---

## Uppgift 2: favoritfärg
**Svårighetsgrad:** ⭐⭐ (lite svårare)  

**Instruktion:**  
Skapa ett skript som tar emot två argument: **namn och favoritfärg**. Skriptet ska sedan skriva ut ett meddelande som använder båda argumenten.  

**Exempel på körning:**  
```bash
./favoritfärg.sh Alex Blå
```
**Utmatning:**  
```
Hej Alex! Din favoritfärg är Blå.
```
Om användaren glömmer att ange båda argumenten, ska skriptet skriva ut:  
```
Du måste ange både namn och favoritfärg!
```

---

## Uppgift 3: lista flera namn
**Svårighetsgrad:** ⭐⭐⭐ (lite mer tänkande)  

**Instruktion:**  
Skapa ett skript som kan ta emot **flera namn som argument** och skriva ut en hälsning till varje person. Skriptet ska även visa hur många namn som angavs.  

**Exempel på körning:**  
```bash
./lista_namn.sh Alice Bob Charlie
```
**Utmatning:**  
```
Du angav 3 namn.
Hej, Alice!
Hej, Bob!
Hej, Charlie!
```
Om skriptet körs utan argument, ska det skriva ut:  
```
Du måste ange minst ett namn!
```

---

## Uppgift 4: numrerad lista av namn
**Svårighetsgrad:** ⭐⭐⭐⭐ (mer logik)  

**Instruktion:**  
Skapa ett skript som fungerar som **Uppgift 3**, men numrerar varje namn.  

**Exempel på körning:**  
```bash
./numrerad_lista.sh Anna Erik Lisa
```
**Utmatning:**  
```
1. Hej, Anna!
2. Hej, Erik!
3. Hej, Lisa!
```
Om inga argument anges, ska det skriva ut:
```
Du måste ange minst ett namn!
```

**Tips:** Använd en räknare som ökar i loopen.

---

## Uppgift 5: summera tal
**Svårighetsgrad:** ⭐⭐⭐⭐⭐ (mer avancerad)  

**Instruktion:**  
Skapa ett skript som tar emot **flera tal som argument** och summerar dem. Om inga tal anges, ska skriptet skriva ut:  
```
Du måste ange minst ett tal!
```

**Exempel på körning:**  
```bash
./summa.sh 5 10 15
```
**Utmatning:**  
```
Summan av talen är: 30
```

**Tips:** Använd en `for`-loop för att addera alla argument.

