# Installera Linux Ubuntu

![alt text](../.gitbook/assets/image-26.png)

## Installera Ubuntu med WSL

1. Öppna **PowerShell** som administratör.
2. Kör kommandot `wsl --install`.
3. Starta om datorn.
4. Starta **Ubuntu** från **Start-menyn**.
5. Skapa ett användarkonto och lösenord (detta är inte samma som ditt Windows-konto).
  * Tips: Använd ditt förnamn som användarnamn i små bokstäver.
  * Tips: Använd ditt efternamn som lösenord i små bokstäver.


