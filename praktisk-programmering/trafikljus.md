---
description: Skapa din egen miniövergångsställe med lysdioder och en tryckknapp
---

# Labb 2 - trafikljus

Microcontrollers kan hittas i nästan alla elektroniska produkter som du använder på en daglig grund - inklusive trafikljus. En trafikljusstyrenhet är ett specialbyggt system som ändrar lamporna på en timer, klockor för fotgängare som vill korsa och kan till och med justera tidpunkten för lamporna beroende på hur mycket trafik det finns - prata med närliggande trafikljussystem för att säkerställa hela trafiknätet fortsätter att flyta smidigt. Även om byggandet av ett storskaligt trafikledningssystem är ett ganska avancerat projekt, är det enkelheten i sig att bygga en miniatyrsimulator som drivs av din Raspberry Pi Pico. Med det här projektet ser du hur du styr flera lysdioder, ställer in olika tidpunkter och hur du övervakar en knapptryckning medan resten av programmet fortsätter att köra med en teknik som kallas trådning.

För detta projekt behöver du din Pico; en kopplingsdäck; en röd, en gul eller gul, och en grön lysdiod; tre 330Ω motstånd; en aktiv piezoelektrisk summer; och ett urval av hane-hane (M2M) bygelkablar. Du behöver också en mikro-USB kabel och för att ansluta din Pico till din Raspberry Pi eller annan dator som kör Thony IDE.

## Ett enkelt trafikljus

Börja med att bygga ett enkelt trafikljussystem, som visas i figur 5-1. Ta din röda lysdiod och sätt in den i kopplingsdäckn så att den går över mittdelningen. Använd ett av 330Ω motstånden och en bygelkabel om du behöver göra en längre anslutning för att ansluta det längre anoden på lysdioden till pinnen längst ner till vänster på din Pico sett uppifrån med mikro-USB kabeln överst, GP15. Om du använder en numrerad kopplingsdäck och har din Pico införd längst upp, blir detta kopplingsdäck rad 20.

![Figur 5-1](<../.gitbook/assets/image (30).png>)

Ta en bygelkabel och anslut det kortare benet - katoden - på den röda lysdioden till kopplingsdäckns jordskena. Ta en till och anslut jordskenan till en av dina Picos jordpin (GND) i figur 5-1 har vi använt jordpinnen på rad tre i brödbrädet.

Du har nu en lysdiod ansluten till din Pico, men ett riktigt trafikljus har minst två till för totalt tre: ett rött ljus för att berätta för trafiken att stanna, ett gult eller gult ljus för att berätta för trafiken att ljuset är håller på att förändras och en grön lysdiod för att berätta för trafiken att den kan gå igen.

Ta din bärnstensfärgade eller gula lysdiod och koppla den till din Pico på samma sätt som den röda lysdioden, se till att det kortare benet är det som ansluter till brödskivans jordskena och att du har 330Ω motståndet på plats för att skydda den. Den här gången, dock, kablar det längre benet - via motståndet - till pinnen bredvid det som du kopplade den röda lysdioden till, GP14.

Slutligen, ta den gröna lysdioden och dra upp den på samma sätt igen - kom ihåg 330Ω -motståndet - för att fästa GP13. Det här är dock inte pinnen bredvid pin GP14 - den pinnen är en slipad (GND) pin, som du kan se om du tittar noga på din Pico: jordpinnar har alla en fyrkantig form på sina kuddar, medan andra pin är runda.

När du är klar bör din krets matcha figur 5-1: en röd, en gul eller gul eller en grön lysdiod, alla anslutna till olika GPIO-pin på din Pico via individuella 330Ω motstånd och anslutna till en delad jordpin via din brödbrädets jordskena.

För att programmera dina trafikljus, anslut din Pico och starta **Thony**. Skapa ett nytt program och spara ditt program som **Traffic\_Lights.py. B**örja med att importera maskinbiblioteket så att du kan styra din Picos GPIO-pin:

```python
from machine import Pin
```

Du måste också importera **utime**-biblioteket, så att du kan lägga till förseningar mellan lamporna som tänds och släcks:

```python
import utime
```

Som med alla program som använder din Picos GPIO-pin måste du ställa in varje pin innan du kan styra det:

```python
led_red = Pin(15 , Pin.OUT)
led_amber = Pin(14 , Pin.OUT)
led_green = Pin(13 , Pin.OUT)
```

Dessa rader ställer in pinnen GP15, GP14 och GP13 som utgångar, och var och en får ett beskrivande namn för att göra det lättare att läsa koden: 'led', så att du vet att pinnen styr en lysdiod och sedan färgen på lysdioden .

Riktiga trafikljus kör inte igenom en gång och stannar - de fortsätter, även när det inte finns någon trafik där och alla sover. Så att ditt program gör detsamma måste du skapa en oändlig loop:

```python
while True:
```

Var och en av raderna under detta måste indragas med **TAB**, så MicroPython vet att de utgör en del av loopen; när du trycker på **ENTER** kommer Thony automatiskt att dra in raderna åt dig.

```python
led_red.on()
utime.sleep(5)
led_amber.on()
utime.sleep(2)
led_red.off()
led_amber.off()
led_green.on()
utime.sleep(5)
led_green.off()
led_amber.on()
utime.sleep(5)
led_amber.off()
```

Klicka på **Run**. Titta på lysdioderna: först tänds den röda lysdioden och säger att trafiken ska stanna; Därefter tänds den gula lysdioden för att varna förarna att lamporna håller på att ändras; nästa slocknar båda lysdioderna och den gröna lysdioden tänds för att låta trafiken veta att den kan passera; sedan slocknar den gröna lysdioden och den gula tänds för att varna förarna att lamporna håller på att bytas igen; slutligen slocknar den gula lysdioden - och loopen startar om från början, med den röda lysdioden tänd.

Mönstret kommer att loopas tills du trycker på stoppknappen, eftersom det bildar en oändlig loopen. Den är baserad på trafikljusmönstret som används i verkliga trafikkontrollsystem i Storbritannien och Irland, men skyndade på-att ge bilar bara fem sekunder att passera genom lamporna skulle inte låta trafiken flöda särskilt fritt!

Riktiga trafikljus finns dock inte bara för vägfordon: de är också där för att skydda fotgängare, vilket ger dem möjlighet att säkert korsa en trafikerad väg.

För att förvandla dina trafikljus till en lunnekorsning behöver du två saker: en tryckknappsbrytare, så att fotgängaren kan be lamporna att låta dem korsa vägen; och en summer, så fotgängaren vet när det är deras tur att korsa. Anslut dem till din kopplingsdäck som i Figur 5-2 , med omkopplaren ansluten till GP16 och 3V3-skenan på din kopplingsdäck, och summern ansluten till GP12 och jordskenan på din kopplingsdäck.

![Figur 5-2](<../.gitbook/assets/image (31).png>)

Om du kör ditt program igen hittar du knappen och summern gör ingenting. Det beror på att du ännu inte har berättat för ditt program hur du använder dem. I Thony, gå tillbaka till raderna där du ställer in dina lysdioder och lägg till följande två nya rader nedan:

```python
knapp = Pin(16 , Pin.IN, Pin.PULL_DOWN)
summer = Pin(12 , Pin.OUT)
```

Detta ställer in knappen på pin GP16 som en ingång och summern på pinnen GP12 som en utgång. Kom ihåg att din Raspberry Pi Pico har inbyggda programmerbara motstånd för sina ingångar, som vi ställer in för neddragningsläge för projekten i den här boken. Det betyder att pinnens spänning dras ner till 0 V (och dess logiska nivå är 0), såvida den inte är ansluten till 3,3V effekt (i så fall kommer dess logiska nivå att vara 1 tills den är frånkopplad).

Därefter behöver du ett sätt för ditt program att ständigt övervaka knappens värde. Tidigare har alla dina program arbetat steg-för-steg genom en lista med instruktioner-bara någonsin gjort en sak i taget. Ditt trafikljusprogram är inte annorlunda: när det körs går MicroPython igenom dina instruktioner steg för steg, tänder och släcker lysdioderna.

För en grundläggande uppsättning trafikljus är det tillräckligt; för en lunnekryssning måste ditt program dock kunna spela in om knappen har tryckts in på ett sätt som inte avbryter trafikljusen. För att få det att fungera behöver du ett nytt bibliotek: **\_thread**. Gå tillbaka till avsnittet i ditt program där du importerar maskinen och **utime**-bibliotek och importerar **\_thread**-biblioteket:

```python
import _thread
```

En tråd eller exekveringstråd är i själva verket ett litet och delvis oberoende program. Du kan tänka på loopen du skrev tidigare, som styr lamporna, som huvudtråden i ditt program - och med **\_thread**-biblioteket kan du skapa en ytterligare tråd som körs samtidigt.

Ett enkelt sätt att visualisera trådar är att tänka på var och en som en separat arbetare i ett kök: medan kocken förbereder huvudrätten arbetar någon annan på en sås. För tillfället har ditt program bara en tråd - den som styr trafikljusen. RP2040-mikrokontrollern som driver din Pico har dock två bearbetningskärnor - vilket innebär att du, precis som kocken och souschefen i köket, kan köra två trådar samtidigt för att få mer arbete gjort.

Innan du kan skapa en annan tråd behöver du ett sätt för den nya tråden att skicka information tillbaka till huvudtråden - och du kan göra detta med hjälp av globala variabler . De variabler du har arbetat med innan detta kallas lokala variabler och fungerar bara i en del av ditt program. en global variabel fungerar överallt, vilket innebär att en tråd kan ändra värdet och en annan kan kontrollera om den har ändrats.

För att börja måste du skapa en global variabel. Lägg till följande nedanför din summer = rad:

```python
global knapp_tryckt
knapp_tryckt = False
```

Detta ställer in **knapp\_tryckt** som en global variabel och ger det ett standardvärde **False** - vilket betyder att när programmet startar har knappen ännu inte tryckts in. Nästa steg är att definiera din tråd, genom att lägga till följande rader direkt nedan - lägga till en tom rad, om du vill, för att göra ditt program mer läsbart:

```python
def button_reader_thread():
    global knapp_tryckt 
    while True:
        if button.value() == 1 :
            knapp_tryckt = True
            utime.sleep(0,01)
```

Den första raden du har lagt till definierar din tråd och ger den ett beskrivande namn: det är en tråd för att läsa knappinmatningen. Precis som när du skriver en loop, behöver MicroPython att allt som finns i tråden ska indragas av fyra mellanslag - så det vet var tråden börjar och slutar.

Nästa rad låter MicroPython veta att du kommer att ändra värdet på den globala **knappen\_tryckt** variabel. Om du bara vill kontrollera värdet skulle du inte behöva denna rad - men utan den kan du inte göra några ändringar i variabeln.

Därefter har du skapat en ny loopen-vilket innebär att en ny fyra-strecksats måste följa, för åtta totalt, så MicroPython vet både att loopen är en del av tråden och koden nedan är en del av loopen. Denna kapsling av kod i flera indragningsnivåer är mycket vanligt i MicroPython, och Thony kommer att göra sitt bästa för att hjälpa dig genom att automatiskt lägga till en ny nivå varje gång det behövs - men det är upp till dig att komma ihåg att ta bort de mellanslag det lägger till när du är klar med en viss del av programmet.

Nästa rad är ett villkor som kontrollerar om knappens värde är 1. Eftersom din Pico använder ett internt neddragningsmotstånd, när knappen inte trycks ned är värdet läst 0, vilket betyder koden under villkorligt körs aldrig. Först när knappen trycks in kommer den sista raden i din tråd att köras: en rad som sätter **knappen\_tryckt** variabel till **True** , så att resten av ditt program vet att knappen har tryckts in. Slutligen lägger vi till en mycket kort (0,01 sekund) fördröjning för att förhindra medan loop går för snabbt.

Du kanske märker att det inte finns något i tråden för att återställa **knappen\_tryckt** variabel till **False** när knappen släpps efter att den har tryckts in. Det finns en anledning till det: medan du kan trycka på knappen för en lunnekryssning när som helst under trafikljuscykeln, träder den bara i kraft när ljuset har blivit rött och det är säkert för dig att korsa. Allt din nya tråd behöver göra är att ändra variabeln när knappen har tryckts in; din huvudtråd hanterar att återställa den till **False** när fotgängaren säkert har korsat vägen.

Att definiera en tråd gör inte att den körs: det är möjligt att starta en tråd när som helst i programmet, och du måste specifikt berätta för **\_thread**-biblioteket när du vill starta tråden. Till skillnad från att köra en vanlig kodrad, stoppar inte köra tråden resten av programmet: när tråden startar fortsätter MicroPython och kör nästa rad i ditt program även om den kör den första raden i din nya tråd.

Skapa en ny rad under din tråd, radera hela den indragning som Thony automatiskt har lagt till för dig, som lyder:

Detta berättar **\_thread**-biblioteket för att starta tråden som du definierade tidigare. Vid denna tidpunkt börjar tråden att köras och går snabbt in i loopen - kontrollera knappen tusentals gånger i sekunden för att se om den har tryckts ännu. Huvudtråden fortsätter under tiden med huvuddelen av ditt program.

Klicka på **Run** nu. Du kommer att se trafikljusen fortsätta sitt mönster precis som tidigare, utan fördröjning eller pauser. Om du trycker på knappen kommer inget att hända - eftersom du inte har lagt till koden för att faktiskt reagera på knappen ännu.

Gå till början av din huvudloopen, direkt under raden **while True:**, och lägg till följande kod - kom ihåg att vara uppmärksam på den kapslade fördjupningen och radera indragningen som Thony har lagt till när det inte längre krävs:

```python
if knapp_tryckt == True:
    led_red.on()
    for i in range(10):
        summer.on()
        utime.sleep(0,2)
        summer.off()
        utime.sleep(0,2)
    global knapp_tryckt
    knapp_tryckt = False
```

Den här delen av koden kontrollerar den globala variabeln med knappar för att se om tryckknappen har tryckts någon gång sedan loopen senast kördes. Om den har, som rapporterats av knappläsningstråden du gjorde tidigare, börjar den köra en koddel som börjar med att slå på den röda lysdioden för att stoppa trafiken och sedan pipa summern tio gånger - så att fotgängaren vet att det är dags att korsa.

Slutligen återställer de två sista raderna **knappen\_tryckt** variabel tillbaka till False - så nästa gång loopen körs kommer den inte att utlösa övergångskoden om inte knappen har tryckts in igen. Du ser att du inte behövde raden global **knappen\_tryckt** för att kontrollera variabelns status i villkorlig; det behövs bara när du vill ändra variabeln och få den ändringen att påverka andra delar av ditt program.

Ditt program ska se ut så här:

```python
import maskin
import utime
import _thread

led_red = Pin(15 , Pin.OUT)
led_amber = Pin(14 , Pin.OUT)
led_green = Pin(13 , Pin.OUT)
knapp = Pin(16 , Pin.IN, Pin.PULL_DOWN)
summer = Pin(12 , Pin.OUT)

global knapp_tryckt knapp_tryckt = False

def button_reader_thread():
    global knapp_tryckt 
    while True: 
        if button.value() == 1:
            knapp_tryckt = True
            utime.sleep(0,01)
_thread.start_new_thread(button_reader_thread,())

while True: 
    if knapp_tryckt == True:
        led_red.on()
        for i in range(10):
            summer.on() 
            utime.sleep(0,2) 
            summer.off() 
            utime.sleep(0,2) 
        global knapp_tryckt 
        knapp_tryckt = False

    led_red.on()
    utime.sleep(5) 
    led_amber.on() 
    utime.sleep(2) 
    led_red.off() 
    led_amber.off() 
    led_green.on() 
    utime.sleep(5) 
    led_green.off() 
    led_amber.on() 
    utime.sleep(5) 
    led_amber.off()
```

Klicka på **Run**. Först kommer programmet att köra som vanligt: ​​trafikljusen tänds och släcks i det vanliga mönstret. Tryck på tryckknappen: om programmet för närvarande är mitt i loopen kommer ingenting att hända förrän det når slutet och slingrar tillbaka igen-kl.

vilken punkt lampan blir röd och summern piper för att meddela att det är säkert att korsa vägen. Den villkorade delen av koden för att korsa vägen körs innan koden du skrev tidigare för att tända och släcka lamporna i ett cykliskt mönster: när det är klart börjar mönstret som vanligt med den röda lysdioden som lyser i ytterligare fem sekunder på toppen av tiden var det tänt medan summern gick. Detta efterliknar hur en riktig lunnekorsning fungerar: det röda ljuset förblir tänt även efter att summern har slutat låta, så alla som började korsa vägen medan summern höll på att nå den andra sidan innan trafiken får gå.

Låt trafikljusen gå igenom cykeln några gånger till och tryck sedan på knappen igen för att utlösa en ny korsning. Grattis: du har byggt din egen lunnekorsning!

{% hint style="info" %}
**UTMANING: KAN DU FÖRBÄTTA DET?**\
Kan du ändra programmet för att ge fotgängaren längre tid korsa?\
Kan du hitta information om andra länders trafik ljusmönster och omprogrammera dina lampor för att matcha?\
Kan du lägg till en andra knapp, så fotgängaren på andra sidan av vägen kan signalera att de också vill korsa?
{% endhint %}
