# Använd den inbyggda temperatursensorn

I denna guide kommer vi att utforska hur du kan använda den inbyggda temperatursensorn på din mikrokontroller för att mäta omgivningstemperaturen. Att kunna läsa av temperaturdata är grundläggande i många projekt, från väderstationer till smarta hemsystem. Vi börjar med grunderna i att läsa av sensorn och rör oss sedan vidare till mer avancerade applikationer.

## Kom igång med temperatursensorn

För att börja med, behöver vi förstå hur vi kan läsa av den inbyggda temperatursensorn och tolka dess data. Nedanstående exempel visar hur du kan få en enkel temperaturavläsning i Celsius:

```python
# Importera bibliotek
from machine import ADC

# Skapa ett ADC-objekt
temp_sensor = ADC(4)

# Konverteringsfaktor för temperatursensorn
conversion_factor = 3.3 / (65535)

# Läs av sensorn
reading = temp_sensor.read_u16() * conversion_factor

# Omvandla till Celsius (formeln varierar beroende på mikrokontroller)
temperature = 27 - (reading - 0.706)/0.001721

print(temperature)
```

### Förstå koden

1. **ADC(4):** Vi initierar ADC (Analog till Digital Converter) på den kanal som temperatursensorn är kopplad till. Numret kan variera beroende på din mikrokontroller.
2. **Läs av och omvandla:** Vi läser av sensorns värde och omvandlar det till en temperatur i Celsius genom en specifik formel. Denna formel kan variera beroende på din enhet, så det är viktigt att referera till din mikrokontrollers dokumentation.

## Upprepad temperaturavläsning

För att kontinuerligt övervaka temperaturen kan du använda en loop för att upprepa avläsningen med jämna intervaller. Här är ett exempel på hur du kan uppdatera temperaturen varje sekund:

```python
from machine import ADC
import utime

temp_sensor = ADC(4)
conversion_factor = 3.3 / (65535)

while True:
    reading = temp_sensor.read_u16() * conversion_factor
    temperature = 27 - (reading - 0.706)/0.001721
    print("Temperaturen är:", temperature , "°C")
    utime.sleep(1)
```

## Experiment med temperatursensorn

Här följer några uppgifter för att ge dig praktisk erfarenhet av att använda den inbyggda temperatursensorn:

1. **Logga temperaturdata:** Skriv en kod som loggar temperaturen med jämna intervaller. Spara datan i en fil eller skriv ut den i konsolen.

2. **Temperaturlarm:** Ställ in ett temperaturområde. Om temperaturen går över eller under detta intervall, låt programmet skicka en visuell eller ljudsignal.

3. **Temperaturbaserad fläktkontroll:** Om du har tillgång till en extern fläkt kopplad till mikrokontrollern, använd temperaturavläsningen för att styra fläktens hastighet baserat på den uppmätta temperaturen.

4. **Bygg en enkel väderstation:** Kombinera temperatursensorn med andra sensorer (som en luftfuktighetssensor) för att skapa en grundläggande väderstation som kan visa aktuella väderdata.

5. **Visualisera temperaturdata:** Skicka temperaturdata från din mikrokontroller till en dator eller en mobil enhet och använd ett program för att visualisera temperaturförändringar över tid.

6. **Temperaturbaserat bevattningssystem:** För ett projekt inom smart hem eller trädgårdsskötsel, använd temperaturdata för att kontrollera ett bevattningssystem. Högre temperaturer kan triggera mer frekvent bevattning.

7. **Minimi och maximi temperaturlogg:** Modifiera din kod för att kontinuerligt spåra och spara den lägsta och högsta uppmätta temperaturen under en given period.

8. **Implementera en hysteresfunktion:** För att förhindra alltför frekventa ändringar (som att slå på/av en fläkt) baserat på små temperaturfluktuationer, implementera en hysteresfunktion som kräver en större temperaturförändring innan åtgärder vidtas.

9. **Kombinera med en tidsbaserad händelse:** Använd temperatursensorn tillsammans med en realtidsklocka (RTC) för att aktivera specifika händelser vid vissa tider på dagen, beroende på temperaturen.

10. **Skapa en interaktiv temperaturkarta:** För ett avancerat projekt, använd flera mikrokontrollers med temperatursensorer placerade på olika platser. Samla och analysera data för att skapa en temperaturkarta över ett område.

Genom att utforska dessa uppgifter kommer du att få värdefull praktisk erfarenhet av att arbeta med temperatursensorer och lära dig hur dessa kan integreras i större system för olika tillämpningar.