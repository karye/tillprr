# Läsa av en knapp

![Figur 4-4](<../../.gitbook/assets/image (28).png>)

## Ingångar: läsa av en knapp

Att läsa av en knapp är en av de mest grundläggande uppgifterna för en mikrokontroller. Det är också en av de mest användbara: knappar används i allt från dörrklockor till fjärrkontroller till spelkonsoler. Att läsa av en knapp är enkelt: du behöver bara en knapp och en pinne som kan läsa av den.

```python
# Importera bibliotek
from machine import Pin

# Använd GP14 för att läsa av en tryckknapp
knapp = Pin(14, Pin.IN, Pin.PULL_DOWN)

# Skriv ut värdet på knappen
print(knapp.value())
```

### Läsa av en knapp kontinuerligt

För att läsa knappen kontinuerligt måste du lägga till en loop i ditt program. Redigera programmet så att det lyder enligt nedan:

```python
# Importera bibliotek
from machine import Pin
import utime

# Använd GP14 för att läsa av en tryckknapp
knapp = Pin(14, Pin.IN, Pin.PULL_DOWN)

# En oändlig loop
while True :
    # Om knappen är nedtryckt
    if knapp.value() == 1 :
        # Skriv ut värdet på knappen
        print("Du tryckte på knappen!")

        # Vänta 2 sekunder
        utime.sleep(2)
```

Du ser meddelandet skrivas ut varje gång du trycker på knappen. Om du håller knappen intryckt längre än fördröjningen på två sekunder, skrivs meddelandet ut varannan sekund tills du släpper knappen.

## Inputs och outputs: att sätta ihop allt

De flesta kretsar har mer än en komponent, varför din Pico har så många GPIO-pin. Det är dags att sätta ihop allt du har lärt dig för att bygga en mer komplex krets: en enhet som slår på och av en LED med en knapp.

![Figur 4-5](<../../.gitbook/assets/image (29).png>)

I själva verket kombinerar denna krets båda de tidigare kretsarna till en. Du kanske kommer ihåg att du använde pin GP15 för att driva den externa lysdioden och pin GP14 för att läsa knappen; bygg om nu din krets så att både lysdioden och knappen är på kopplingsdäcken samtidigt, fortfarande anslutna till GP15 och GP14 (figur 4-5). Glöm inte det strömbegränsande motståndet för lysdioden!

```python
# Importera bibliotek
from machine import Pin, utime

# Använd GP15 för att styra en lysdiod
led_extern = Pin(15 , Pin.OUT)

# Använd GP14 för att läsa av en tryckknapp
knapp = Pin(14 , Pin.IN, Pin.PULL_DOWN)

# En oändlig loop
while True :
    # Om knappen är nedtryckt
    if knapp.value() == 1 :
        # Sätt utgångspinnen till 1 (tänd)
        led_extern.on()

        # Vänta 2 sekunder
        utime.sleep(2)
    else :
        # Sätt utgångspinnen till 0 (släck)
        led_extern.off()
```

## Uppgifter

1. **Lägg till en till lysdiod**. Använd en annan GPIO-pin för att styra en annan lysdiod. Låt den andra lysdioden lysa när knappen är nedtryckt och släck när knappen släpps.
2. **Lägg till en summer**. Använd en annan GPIO-pin för att styra en summer. Låt summern låta när knappen är nedtryckt och tystna när knappen släpps.
