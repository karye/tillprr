---
description: >-
  Lär dig mer om din Raspberry Pi Pico's pinnar och elektroniska komponenter som
  du kan ansluta och styra
---

# Programmera Pico

![alt text](../../.gitbook/assets/image-1.png)

När människor tänker på 'programmering' eller 'kodning', tänker de är oftast på mjukvara. Kodning kan dock handla om mer än bara programvara: den kan påverka den verkliga världen genom hårdvara. Detta är känt som **industriell programmering**. Som namnet antyder handlar det om fysisk dator som kan styra saker i den verkliga världen med program. När du ställer in programmet på din tvättmaskin, ändrar temperaturen på din programmerbara termostat eller trycker på en knapp vid trafikljusen för att korsa vägen säkert, använder du **industriell programmering**.

Dessa enheter styrs vanligtvis av en **mikrokontroller** ungefär som den på din Raspberry Pi Pico - och det är fullt möjligt för dig att skapa dina egna **styrsystem** genom att lära dig att dra fördel av dina Picos möjligheter, lika enkelt som du lärde dig att skriva programvara.

## Din Picos pinnar

Din Pico kommunicerar med hårdvara genom en serie pinar längs båda kanterna. De flesta av dessa pinnar fungerar som en ingång/utgång (**GPIO**) för allmänna ändamål, vilket innebär att de kan programmeras att antingen fungera som en **ingång** eller en **utgång** och inte har något eget fast syfte. Vissa pinnar har extra funktioner och alternativa lägen för att kommunicera med mer komplicerad hårdvara; andra har ett fast syfte och ger anslutningar för saker som ström.

Raspberry Pi Picos 40 pinnar är märkta på undersidan av brädet. Tre pinnar är också märkta med sina nummer ovanpå brädan: Pin 1, Pin 2 och Pin 39. Dessa toppetiketter hjälper dig att komma ihåg hur numreringen fungerar: Pin 1 är längst upp till vänster när du tittar på brädet uppifrån med mikro-USB porten till ovansidan, Pin 20 är längst ned till vänster, Pin 21 längst ner till höger och Pin 39 en under överst till höger med den omärkta Pin 40 ovanför den.

![Figur 3-1](<../../.gitbook/assets/image (23).png>)

[pico.pinout](https://pico.pinout.xyz/)

I stället för att använda de fysiska pinnumren är det dock vanligare att referera till pinnen med funktionerna på varje (se figur 3-1). Det finns flera kategorier av pintyper, som alla har en särskild funktion:

* **VSYS**-ingången. Denna strömförsörjning kan slås på och av med 3V3\_EN-pinnen ovanför, vilket också stänger av din Pico.
* En pin direkt ansluten till din Pico interna strömförsörjning, som inte kan stängas av utan att även stänga av Pico.
* En källa till 5V ström som tas från din Picos mikro-USB port och används för att driva hårdvara som behöver mer än 3,3V.
* En jordanslutning, som används för att slutföra en krets som är ansluten till en strömkälla. Flera av dessa pin är prickade runt din Pico för att göra ledningen enklare.
* **GPxx** GPIO-pinnen som är tillgängliga för ditt program, märkta 'GP0' till 'GP28'.
* En GPIO-pin som slutar med 'ADC' och ett nummer GP xx \_ADC kan användas som en analog ingång såväl som en digital ingång eller utgång - men inte båda samtidigt.
* **ADC\_VREF** En speciell ingångspin som anger en referensspänning för alla analoga ingångar.
* **AGND** En speciell jordanslutning för användning med ADC\_VREF-pinnen.
* **RUN** aktiverar eller inaktiverar din Pico. RUN-rubriken används för att starta och stoppa din Pico från en annan mikrokontroller.

Flera av GPIO-pinnen har ytterligare funktioner, som du lär dig mer om senare i boken. För en fullständig överblick inklusive dessa ytterligare funktioner, se bilaga B.

## Elektroniska komponenter

Din Pico är bara en del av det du behöver för att börja arbeta med **industriell programmering**; den andra halvan består av elektriska komponenter, enheterna du kommer att styra från Picos GPIO -pin. Det finns tusentals olika komponenter tillgängliga, men de flesta fysiska dataprojekt görs med hjälp av följande gemensamma delar.

![](<../../.gitbook/assets/image (12).png>)

En kopplingsdäck, även känd som en lödlös kopplingsdäck, kan göra fysiska dataprojekt betydligt enklare. I stället för att ha en massa separata komponenter som måste anslutas med ledningar, kan du använda en kopplingsdäck för att sätta in komponenter och ha dem anslutna genom metallspår som är dolda under ytan. Många kopplingsdäck innehåller också sektioner för kraftfördelning, vilket gör det lättare att bygga dina kretsar. Du behöver ingen kopplingsdäck för att komma igång med fysisk databehandling, eftersom du kan använda speciella trådar för att ansluta komponenter direkt till din Picos GPIO-pin, men det gör verkligen saker enklare och stabilare.

![](<../../.gitbook/assets/image (13).png>)

Bygelkablar, även kända som bygelkablar , ansluter komponenter till din Pico och, om du inte använder en kopplingsdäck, till varandra. De finns i tre versioner: hane-hane (M2F); hona-hona (F2F), som kan användas för att ansluta enskilda komponenter till din Pico om du inte använder en kopplingsdäck; och hane-hane (M2M), som används för att göra anslutningar från en del av en kopplingsdäck till en annan. Beroende på ditt projekt kan du behöva alla tre typer av bygeltråd; om du använder en kopplingsdäck kan du oftast komma undan med bara M2F- och M2M-bygelkablar.

![](<../../.gitbook/assets/image (14).png>)

En tryckknappsbrytare, även känd som en tillfällig omkopplare, är den typ av omkopplare du skulle använda för att styra en spelkonsol. Vanligtvis tillgänglig med två eller fyra ben-båda typerna fungerar med din Pico-tryckknappen är en inmatningsenhet: du kan berätta för ditt program att se upp för att det trycks och sedan utföra en uppgift. En annan vanlig omkopplare är en spärrbrytare; medan en tryckknapp bara är aktiv medan du håller den nedtryckt, aktiveras en spärrkontakt- som du skulle hitta i en strömbrytare-när du växlar den en gång och förblir sedan aktiv tills du växlar igen.

![](<../../.gitbook/assets/image (15).png>)

En ljusdiod (LED) är en utgångsenhet; du styr det direkt från ditt program. En lysdiod tänds när den är på och du hittar dem i hela ditt hus, allt från de små som låter dig veta när du har lämnat din tvättmaskin påslagen till de stora du kan tänka lysa upp i dina rum. Lysdioder finns i en mängd olika former, färger och storlekar, men inte alla är lämpliga att använda med din Pico: undvik alla som säger att de är avsedda för 5 V eller 12 V nätaggregat.

![](<../../.gitbook/assets/image (16).png>)

Motstånd är komponenter som styr flödet av elektrisk ström och är tillgängliga i olika värden mätt med en enhet som kallas ohm (Ω). Ju högre antal ohm desto mer motstånd ges. För Pico fysiska dataprojekt är deras vanligaste användning att förhindra att lysdioder drar för mycket ström och skadar sig själva eller din Pico; för detta vill du ha motstånd som är rankade till cirka 330Ω, även om många elleverantörer säljer praktiska förpackningar som innehåller ett antal olika vanliga värden som ger dig mer flexibilitet.

![](<../../.gitbook/assets/image (17).png>)

En piezoelektrisk summer, vanligtvis bara kallad summer eller ett ekolod, är en annan utmatningsenhet. Medan en LED producerar ljus, ger en summer en ljud- ett surrande ljud faktiskt. Inuti summerns plasthölje finns ett par metallplattor; när de slås på vibrerar dessa plattor mot varandra för att producera det surrande ljudet. Det finns två typer av summer: aktiva summer och passiva summer. Se till att skaffa en aktiv summer, eftersom dessa är de enklaste att använda.

![](<../../.gitbook/assets/image (18).png>)

En potentiometer är den typ av komponent du kan hitta som volymkontroll på en musikspelare och kan fungera som två olika komponenter. Med två av sina tre ben anslutna fungerar det som ett variabelt motstånd eller varistor, en typ av motstånd som kan justeras när som helst genom att vrida vredet. Med alla tre benen ordentligt anslutna blir det en spänningsdelare och matar ut allt från 0 V till fullspänningsingången beroende på vredets position.

![](<../../.gitbook/assets/image (19).png>)

En passiv infraröd sensor (PIR) är en av en mängd olika ingångsenheter som kallas sensorer , utformade för att rapportera om förändringar i vad de övervakar. När det gäller en PIR -sensor övervakar den rörelse: sensorn tittar på förändringar i det område som täcks av dess plastlins och skickar en signal när den upptäcker rörelse. PIR -sensorer finns vanligtvis på inbrottslarm för att hitta människor som rör sig i mörkret.

![](<../../.gitbook/assets/image (20).png>)

En I2C-display är en skärm som pratar med din Pico via ett speciellt kommunikationssystem som kallas den inter-integrerade kretsen (I2C)-bussen . Med den här bussen kan din Pico styra bildskärmspanelen och skicka allt från att skriva till bilder så att den kan visas. Det finns många typer av skärmar tillgängliga, men en populär - och den som finns i den här boken - är SerLCD från SparkFun, som har både I2C- och SPI -gränssnitt. Observera att vissa skärmar endast använder seriell perifer gränssnitt (SPI) buss snarare än I2C; de fungerar fortfarande med din Pico, men programmet måste ändras.

Andra vanliga elektriska komponenter inkluderar motorer, som behöver en speciell förarkomponent innan de kan anslutas till din Pico, strömsensorer som kan detektera hur mycket ström en krets använder, tröghetsmätningsenheter (IMU: er) som spårar rörelse och orientering och ljus- beroende motstånd (LDR) - ingångsenheter som fungerar som en omvänd LED genom att detektera ljus i stället för att avge det.

Säljare över hela världen tillhandahåller komponenter för fysisk datoranvändning med Raspberry Pi, antingen som enskilda delar eller i kit som ger allt du behöver för att komma igång. För att hitta säljare, besök [rpf.io/products](https://rpf.io/products) , klicka på Raspberry Pi 4, så får du en lista över Raspberry Pi-partners onlinebutiker (godkända återförsäljare) för ditt land eller din region.

{% hint style="info" %}
**För att slutföra projekten i den här boken bör du ha minst:**\
En Raspberry Pi Pico med manliga rubriker fästa\
En mikro-USB kabel\
En lödlös kopplingsdäck\
En Raspberry Pi eller annan dator för programmering\
Hona-hona (M2F) och hane-hane (M2M) bygelkablar\
3 × enfärgade lysdioder: rött, grönt och gult eller gult\
1 × aktiv piezoelektrisk summer\
1 × 10 kΩ potentiometer, linjär eller logaritmisk\
3 × 330 Ω motstånd\
Minst en HC-SR501 PIR-sensor\
1 × SerLCD-modul\
WS2812B lysdioder
{% endhint %}

Du kommer också att tycka att det är bra att köpa en billig förvaringslåda med flera fack, så att du kan hålla komponenterna du inte använder i ditt projekt säkra och städade. Om du kan, försök hitta en som också passar kopplingsdäcken - på det sättet kan du städa bort allt varje gång du är klar.

### Läser av motståndets färgkoder

Resistorer finns i ett brett spektrum av värden, från versioner med nollmotstånd som faktiskt bara är bitar av trådar till versioner av storleken på ditt ben som används i kraftverk. Mycket få av dessa motstånd har dock sina värden tryckta på dem i siffror: istället använder de en särskild kod tryckt som färgade ränder eller band runt motståndets kropp.

![](<../../.gitbook/assets/image (21).png>)

För att läsa värdet på ett motstånd, placera det så att gruppen av band är till vänster och det ensamma bandet är till höger. Börja med det första bandet, leta upp dess färg i kolumnen "1: a/2: a bandet" i tabellen för att få den första och andra siffran. Detta exempel har två orange band, som båda betyder ett värde på '3' för totalt '33'. Om ditt motstånd har fyra grupperade band istället för tre, notera värdet på det tredje bandet också-och för fem- eller sexbandsmotstånd se rpf.io/5-6band.

Flytta till det sista grupperade bandet - det tredje eller fjärde, beroende på ditt motstånd - leta upp dess färg i kolumnen "Multiplikator". Detta berättar vilket nummer du behöver för att multiplicera ditt nuvarande nummer med för att få det verkliga värdet av motståndet. Detta exempel har ett brunt band, vilket betyder '× 10¹'. Det kan se förvirrande ut, men det är helt enkelt vetenskaplig notering: '× 10¹' betyder helt enkelt 'lägg till en nolla i slutet av ditt tal'. Om det var blått, för × 10 6', skulle det betyda' lägg till sex nollor i slutet av ditt nummer '.

33, från de orangea banden, plus den extra nollan från det bruna bandet ger oss 330 - vilket är värdet på motståndet, mätt i ohm. Det sista bandet, till höger, är motståndets tolerans . Detta är helt enkelt hur nära det nominella värdet det sannolikt kommer att vara. Billigare motstånd kan ha ett silverband, vilket indikerar att det kan vara 10 procent högre eller lägre än dess rating, eller inget sista band alls, vilket indikerar att det kan vara 20 procent högre eller lägre; de dyraste motstånden har ett grått band, vilket indikerar att det kommer att ligga inom 0,05 procent av dess rating. För de flesta hobbyprojekt är noggrannheten inte så viktig: någon tolerans brukar fungera bra.

Om ditt motståndsvärde går över 1000 ohm (1000 Ω), är det vanligtvis klassat i kilo Ohms (kΩ); om det går över en miljon ohm är det mega Ohms (MΩ). Ett 2200 Ω motstånd skulle skrivas som 2,2 kΩ; ett 2200000Ω motstånd skulle skrivas som 2,2 MΩ.
