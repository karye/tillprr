---
description: Installera alla verktyg, konfigurera alla inställningar, skapar alla mappar
---

# Utvecklingsmiljö

## Verktyg för att programmera Pico <a href="#installera-webbeditorn-vs-code-and-tillaegg" id="installera-webbeditorn-vs-code-and-tillaegg"></a>

### Alternativ 1 - Thonny <a href="#installera-webbeditorn-vs-code-and-tillaegg" id="installera-webbeditorn-vs-code-and-tillaegg"></a>

* Installera [Thonny](https://thonny.org/)

![Välj Raspberry Pico](<../.gitbook/assets/image (46).png>)

### Alternativ 2 - VS Code & tillägg <a href="#installera-webbeditorn-vs-code-and-tillaegg" id="installera-webbeditorn-vs-code-and-tillaegg"></a>

* Installera [VS Code](https://code.visualstudio.com/)
* Installera [git-scm](https://git-scm.com/)
* Installera tilläggen
  * [Python Extension Pack](https://marketplace.visualstudio.com/items?itemName=donjayamanne.python-extension-pack)
  * [Beautify](https://marketplace.visualstudio.com/items?itemName=HookyQR.beautify)
  * [Bracket Pair Colorizer 2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)
  * [Pico-Go](https://marketplace.visualstudio.com/items?itemName=ChrisWood.pico-go)

![](../.gitbook/assets/autocomplete.gif)

## Skapa en utvecklingsmiljö <a href="#skapa-en-utvecklingsmiljoe" id="skapa-en-utvecklingsmiljoe"></a>

* Gå till [github.com](https://github.com/) och skapa en konto
* Skapa en mapp **c:/github/tillprog**

![](<../.gitbook/assets/image (7).png>)

* Öppna mappen **c:/github/tillprog** i Thony

![](<../.gitbook/assets/image (8).png>)

* Publicera mappen på [github.com](https://github.com/) med **Publish to Github**

![](<../.gitbook/assets/image (10).png>)
